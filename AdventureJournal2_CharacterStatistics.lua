local name, AJ = ...

function AdventureJournalCharacterStatistics_OnShow()
    AJ.RefreshCharacterStatsUI()
    local class, _, _ = UnitClass('player')
    AdventureJournalCharacterStatisticsFrameCharacterLevelText:SetText(tostring('Level '..UnitLevel('player')..' '..class))
end

function AJ.UpdateCharacterstat(stat, value)
    if AJ2_GlobalVariables[UnitGUID('player')].Settings.CharacterStatistics == nil then
        AJ2_GlobalVariables[UnitGUID('player')].Settings.CharacterStatistics = AJ.CharacterStatistics
    end
    local statAdded = false
    for k, stats in ipairs(AJ2_GlobalVariables[UnitGUID('player')].Settings.CharacterStatistics) do
        if tostring(stats.Key) == tostring(stat) then
            stats.Value = value
            statAdded = true
            --print('updated stat', stat, value)
        end
    end
    if statAdded == false then
        --print('insert new stat', stat, value)
        table.insert(AJ2_GlobalVariables[UnitGUID('player')].Settings.CharacterStatistics, { Key = stat, Value = value} )
    end
end

function AJ.RemoveCharacterStatistic(name)
    if AJ2_GlobalVariables[UnitGUID('player')].Settings.CharacterStatistics ~= nil then
        local removeKey, removeStat = nil, nil
        for k, stat in ipairs(AJ2_GlobalVariables[UnitGUID('player')].Settings.CharacterStatistics) do
            if stat.Key == name then
                removeKey = k
                removeStat = stat.Key
            end
        end
        if removeKey ~= nil then
            table.remove(AJ2_GlobalVariables[UnitGUID('player')].Settings.CharacterStatistics, removeKey)
            print(removeStat)
        end
    end
end

function AJ.GetCharacterstat(stat)
    if AJ2_GlobalVariables[UnitGUID('player')].Settings.CharacterStatistics == nil then
        AJ2_GlobalVariables[UnitGUID('player')].Settings.CharacterStatistics = AJ.CharacterStatistics
    end
    local statValue = nil
    for k, stats in ipairs(AJ2_GlobalVariables[UnitGUID('player')].Settings.CharacterStatistics) do
        if stats.Key == stat then
            statValue = stats.Value
        end
    end
    return statValue
end

AJ.CharacterStatisticsUIFontStrings = {}

function AJ.DrawCharacterStatistics()
    for k, stat in ipairs(AJ2_GlobalVariables[UnitGUID('player')].Settings.CharacterStatistics) do

        local row = AJ.CharStatsRowIDs[stat.Key]
        local xPos, yPos
        if row ~= nil then
            xPos, yPos = 38, (((row * -1) * 25) - 100)
        else
            xPos, yPos = 38, (((k * -1) * 25) - 100)
        end

        local fs_key = AdventureJournalCharacterStatisticsFrame:CreateFontString(tostring('AJ_CharStatsUI_Stat'..k), "OVERLAY", "GameFontNormal_NoShadow")
        fs_key:SetPoint("TOPLEFT", xPos, yPos)
        fs_key:SetText(stat.Key)
        fs_key:SetFont("Fonts\\FRIZQT__.TTF", 14)
        fs_key:SetTextColor(0, 0, 0, 1)

        local fs_value = AdventureJournalCharacterStatisticsFrame:CreateFontString(tostring('AJ_CharStatsUI_Value'..k), "OVERLAY", "GameFontNormal_NoShadow")
        fs_value:SetPoint("TOPLEFT", xPos + 255, yPos)
        fs_value:SetText(stat.Value)
        fs_value:SetFont("Fonts\\FRIZQT__.TTF", 14)
        fs_value:SetTextColor(0, 0, 0, 1)
        fs_value:SetSize(100, 20)
        fs_value:SetJustifyH('RIGHT')

        table.insert(AJ.CharacterStatisticsUIFontStrings, { Stat = stat.Key, StatKeyText = fs_key, StatValueText = fs_value} )

    end
end

function AJ.RefreshCharacterStatsUI()

    for k, statUI in ipairs(AJ.CharacterStatisticsUIFontStrings) do
        if statUI.Stat ~= nil then
            statUI.StatValueText:SetText(AJ.GetCharacterstat(statUI.Stat))

            if statUI.Stat == 'Gold Spent on Repairs' then
                statUI.StatValueText:SetText(GetCoinTextureString(tonumber(AJ.GetCharacterstat(statUI.Stat))))
            elseif statUI.Stat == 'Total Gold Looted' then
                statUI.StatValueText:SetText(GetCoinTextureString(tonumber(AJ.GetCharacterstat(statUI.Stat))))
            elseif statUI.Stat == 'Total Gold Earned' then
                statUI.StatValueText:SetText(GetCoinTextureString(tonumber(AJ.GetCharacterstat(statUI.Stat))))
            end
        end

    end
end

function AJ.AppendNewCharacterStat(statName, value)
    if AJ2_GlobalVariables[UnitGUID('player')] then
        if AJ2_GlobalVariables[UnitGUID('player')].Settings ~= nil then
            if AJ2_GlobalVariables[UnitGUID('player')].Settings.CharacterStatistics ~= nil then
                local statExists = false
                for k, stats in ipairs(AJ2_GlobalVariables[UnitGUID('player')].Settings.CharacterStatistics) do
                    if stats.Key == statName then
                        statExists = true
                    end
                end
                if statExists == false then
                    table.insert(AJ2_GlobalVariables[UnitGUID('player')].Settings.CharacterStatistics, { Key = statName, Value = value })
                    return true
                end
            end
        end
    end
end