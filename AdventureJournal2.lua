local name, AJ = ...

local tinsert = table.insert
local pairs = pairs
local ipairs = ipairs
local UnitGUID = UnitGUID

AJ.NodeGUIDsCreated = {}
AJ.FishingCast = false
AJ.RecentCombatTargets = {}


SLASH_ADVENTUREJOURNAL21 = '/aj2'
SlashCmdList['ADVENTUREJOURNAL2'] = function(msg)
	if msg == 'test' then
		print('test')
		local c = 0
		for i = 1, GetNumTradeSkills() do
			local name, type, _, _, _, _ = GetTradeSkillInfo(i)
			if (name and type ~= "header") then
				c = c + 1
			end
		end
	elseif msg == 'reset-character' then
		local name, realm = UnitName('player')
		StaticPopup_Show("AdventureJournal_ResetCharacter", name)
	elseif msg == 'purge-loot' then
		StaticPopup_Show ("AdventureJournal_PurgeLootDialog")
	elseif msg == 'fix-loot' then
		StaticPopup_Show ("AdventureJournal_FixLootDb")
	elseif msg == 'open' then
		AJ.OpenJournal()
	elseif string.sub(msg, 1, 11) == 'modify-stat' then --can be used to correct a stat for non level 1 characters
		local cmd = {}
		for word in string.gmatch(msg, "%w+" ) do table.insert(cmd, word) end
		local exists, statKey, statValue = false, nil, nil
		if tonumber(cmd[5]) ~= nil then
			statKey = tostring(cmd[3]..' '..cmd[4])
			statValue = tonumber(cmd[5])
		elseif tonumber(cmd[6]) ~= nil then
			statKey = tostring(cmd[3]..' '..cmd[4]..' '..cmd[5])
			statValue = tonumber(cmd[6])
		end
		for k, stats in ipairs(AJ2_GlobalVariables[UnitGUID('player')].Settings.CharacterStatistics) do
			if stats.Key == statKey then
				exists = true
			end
		end
		if exists == true then
			AJ.UpdateCharacterstat(statKey, statValue)
			AJ.RefreshCharacterStatsUI()
			AJ.Print('updated stat successfully: '..statKey..' '..statValue)
		end
		
	end

end

function AJ.CreateMinimapButton()
	local ldb = LibStub("LibDataBroker-1.1")
    AJ.MinimapButton = ldb:NewDataObject('AdventureJournal2', {
        type = "data source",
        icon = 133741,
        OnClick = function(self, button)
            if button == "LeftButton" and not IsAltKeyDown() then
				AJ.OpenJournal()			
			elseif button == 'RightButton' and IsAltKeyDown() then
				if AJ2_GlobalVariables[UnitGUID('player')].Settings.DisplayMinimapIcons == true then
					AJ2_GlobalVariables[UnitGUID('player')].Settings.DisplayMinimapIcons = false
					AJ.ShowMinimapIconsCheckBox:SetChecked(false)
				else
					AJ2_GlobalVariables[UnitGUID('player')].Settings.DisplayMinimapIcons = true
					AJ.ShowMinimapIconsCheckBox:SetChecked(true)
				end
				AJ.UpdateMaps()
			elseif button == 'LeftButton' and IsAltKeyDown() then
				if AJ2_GlobalVariables[UnitGUID('player')].Settings.DisplayWorldMapIcons == true then
					AJ2_GlobalVariables[UnitGUID('player')].Settings.DisplayWorldMapIcons = false
					AJ.ShowIconsWorldMapCheckBox:SetChecked(false)
				else
					AJ2_GlobalVariables[UnitGUID('player')].Settings.DisplayWorldMapIcons = true
					AJ.ShowIconsWorldMapCheckBox:SetChecked(true)
				end
				AJ.UpdateMaps()
			elseif button == 'RightButton' and not IsAltKeyDown() then
				InterfaceOptionsFrame_OpenToCategory('AdventureJournal')
                InterfaceOptionsFrame_OpenToCategory('AdventureJournal')
            end
        end,
        OnTooltipShow = function(tooltip)
            if not tooltip or not tooltip.AddLine then return end
            tooltip:AddLine("|cff0070DEAdventure Journal 2")
            tooltip:AddLine("|cffFFFFFFLeft click:|r Open Journal")
            tooltip:AddLine("|cffFFFFFFRight click:|r Configure")
            tooltip:AddLine("|cffFFFFFFAlt + Right click:|r Toggle Minimap Icons")
            tooltip:AddLine("|cffFFFFFFAlt + Left click:|r Toggle World Map Icons")
            --tooltip:AddLine("|cffFFFFFFRight click:|r Config")
        end,
    })
    AJ.MinimapIcon = LibStub("LibDBIcon-1.0")
    if not AJ.MinimapButton then AJ.MinimapButton = {} end
    AJ.MinimapIcon:Register('AdventureJournal2', AJ.MinimapButton, AJ.MinimapButton)
end

function AdventureJournal_Init()
	if AJ2_GlobalVariables == nil then
		AJ2_GlobalVariables = { AddonName = "AdventureJournal2", Loot = {}, GameObjects = {}, }
	end
	local ticker = C_Timer.After(1, function() --delay this for 1 sec to make sure GUID exists
		if AJ2_GlobalVariables[UnitGUID('player')] == nil then
			AJ2_GlobalVariables[UnitGUID('player')] = {}
		end
		if AJ2_GlobalVariables[UnitGUID('player')].Settings == nil then
			AJ2_GlobalVariables[UnitGUID('player')].Settings = {
				Name = UnitName('player'),
				IgnoreJunkLoot = true, 
				MinimapIconSize = 8.0, 
				HideMinimapPinDistance = 2.5,
				UseMinimapDistanceTracking = false,
				DisplayMinimapIcons = true, 
				DisplayWorldMapIcons = false,
				GuildShare = false, 
				MinimapDisplayObjects = {}, 
				WorldMapDisplayObjects = {},
				NoteColours = AJ.NoteColours,
				TooltipLines = AJ.TooltipLines,
				NoteTracking = AJ.NoteTracking,
				CharacterStatistics = AJ.CharacterStatistics,
			}
		end	
		if AJ2_GlobalVariables[UnitGUID('player')].Notes == nil then
			AJ2_GlobalVariables[UnitGUID('player')].Notes = {}
		end	
		if AJ2_GlobalVariables.GameObjects == nil then
			AJ2_GlobalVariables.GameObjects = {}
		end	
		if AJ2_GlobalVariables.Loot == nil then
			AJ2_GlobalVariables.Loot = {}
		end
		if AJ2_GlobalVariables.QuestXP == nil then
			AJ2_GlobalVariables.QuestXP = {}
		end

		--local regAddonChatPrefix = C_ChatInfo.RegisterAddonMessagePrefix("aj2-quest-XP") --qxp=quest xp

		AJ.DrawLootPage()
		AJ.DrawNotesPage()
		AJ.DrawBagsFrame()
		AJ.DrawCharacterStatistics()

		AJ.TooltipLinesDropDown_Init()
		AJ.MinimapDisplayObjectDropdown_Init()
		AJ.NotesColourDropDown_Init()

		AJ.LoadSettings()
		AJ.CreateMinimapButton()

		--added in 1.0.1
		AJ.AppendNewCharacterStat('Quests Abandoned', 0)

		--added in 1.0.2
		AJ.AppendNewCharacterStat('Bandages Used', 0)

		--error to fix
		AJ.RemoveCharacterStatistic('Quests Aandoned') --spelling error cause multiple entries
	end)
end

function AJ.LoadSettings()
	if AJ2_GlobalVariables[UnitGUID('player')].Settings ~=nil then
		AJ.MinimapIconSizeSlider:SetValue(AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapIconSize)
		AJ.HideMinimapPinDistanceSlider:SetValue(AJ2_GlobalVariables[UnitGUID('player')].Settings.HideMinimapPinDistance)
		AJ.ShowMinimapIconsCheckBox:SetChecked(AJ2_GlobalVariables[UnitGUID('player')].Settings.DisplayMinimapIcons)
		AJ.ShowIconsWorldMapCheckBox:SetChecked(AJ2_GlobalVariables[UnitGUID('player')].Settings.DisplayWorldMapIcons)

		AdventureJournalLootFrameIgnoreJunkCheckBox:SetChecked(AJ2_GlobalVariables[UnitGUID('player')].Settings.IgnoreJunkLoot)
		AdventureJournalLootFrameIgnoreJunkCheckBoxText:SetText("Ignore Junk")
		AdventureJournalLootFrameIgnoreJunkCheckBoxText.tooltip = "Ignore junk when collecting information about your discoveries, will also prevent junk items appearing in your search."
	end
	AJ.UpdateMaps()
end

function AJ.OnUpdate()

--experimental uses a lot of resources at the moment, can it be tuned better
	if AJ2_GlobalVariables[UnitGUID('player')].Settings.UseMinimapDistanceTracking == true then
		local mapID = C_Map.GetBestMapForUnit('player')
		local currentMapPosition = C_Map.GetPlayerMapPosition(mapID, 'player')
		local currentMapName = C_Map.GetMapInfo(mapID).name
		if mapID ~= nil then
			for k, node in ipairs(AJ.MapNodeCacheMinimap) do			
				if node.mapZoneName == currentMapName then
					local inRange, distance = AJ.IsNodeInRange(currentMapPosition.x, currentMapPosition.y, node.posX, node.posY, tonumber(AJ2_GlobalVariables[UnitGUID('player')].Settings.HideMinimapPinDistance))
					if inRange and type(distance) == 'number' then
						node:SetAlpha(0.25)
					else
						node:SetAlpha(1.0)
					end
				end
			end
		end
	end


end

function AJ_OnEvent(self, event, ...)

	if event == "ZONE_CHANGED" or event =="ZONE_CHANGED_INDOORS" or event =="ZONE_CHANGED_NEW_AREA" then
		AJ.UpdateMaps()
	end

	if event == "QUEST_ACCEPTED" then
		AJ.EventsHandler[event]({ QuestID = select(2, ...), QuestName = GetQuestLogTitle(select(1, ...)) })
	end

	if event == "QUEST_TURNED_IN" then
		AJ.EventsHandler[event]({ QuestID = select(1, ...), Money = select(3, ...) })
	end

	if event == "QUEST_REMOVED" then
		AJ.EventsHandler[event]({ QuestID = select(1, ...) })
	end

	if event == "UI_INFO_MESSAGE" then
		if select(1, ...) == 372 then
			AJ.EventsHandler[event]({ ZoneName = select(2, ...) })
		end
	end

	if event == "UNIT_SPELLCAST_SENT" then
		AJ.EventsHandler[event]({ Target = select(2, ...), Spell = select(4, ...) })
	end

	if event == "COMBAT_LOG_EVENT" then 
		AJ.EventsHandler[event]()
	end

	if event == "PLAYER_REGEN_DISABLED" then
		self:SetAlpha(0.2)
	end

	if event == "PLAYER_REGEN_ENABLED" then
		self:SetAlpha(1.0)
	end

	if event == 'LOOT_OPENED' then
		AJ.EventsHandler[event]()
	end

	if event == "PLAYER_DEAD" then
		AJ.EventsHandler[event]()
	end

	if event == ("NEW_RECIPE_LEARNED") then
		local spellID = select(1, ...)
		local name, rank, icon, castTime, minRange, maxRange, spellId = GetSpellInfo(spellID)
	end

	if event == "CHAT_MSG_MONEY" then
		AJ.EventsHandler[event]({ Message = select(1, ...)})
	end

	if event == 'CHAT_MSG_ADDON' then
		if select(1, ...) == 'aj2-quest-XP' then
			AJ.EventsHandler[event]({ Message = select(2, ...)})
		end
	end

end

local updateFrame = CreateFrame("FRAME", "AJ_OnUpdateFrame", UIParent)
updateFrame:SetScript('OnUpdate', AJ.OnUpdate)

AJ.RepairAllCost = 0
MerchantFrame:HookScript('OnShow', function() 
	local repairAllCost, canRepair = GetRepairAllCost()
	AJ.RepairAllCost = repairAllCost
end)

--this fires twice ??? so cancel value
MerchantRepairAllButton:HookScript('OnClick', function()	
	--print('start',AJ.RepairAllCost)
	local stat = AJ.GetCharacterstat('Gold Spent on Repairs')
	AJ.UpdateCharacterstat('Gold Spent on Repairs', AJ.RepairAllCost + stat)
	AJ.RefreshCharacterStatsUI()
	AJ.RepairAllCost = 0
	--print('end',AJ.RepairAllCost)
end)


for i = 1, 6 do
_G['QuestLogTitle'..i]:HookScript('OnClick', function() GameTooltip:Hide() AJ.GetQuestLogInfo() end)
end

function AJ.GetQuestLogInfo()
	local questIndex = GetQuestLogSelection()
	local title, level, suggestedGroup, isHeader, isCollapsed, isComplete, frequency, questID, startEvent, displayQuestID, isOnMap, hasLocalPOI, isTask, isBounty, isStory, isHidden, isScaling = GetQuestLogTitle(questIndex)
	if AJ2_GlobalVariables ~= nil and AJ2_GlobalVariables.QuestXP ~= nil then
		local _xp
		for qid, xp in pairs(AJ2_GlobalVariables.QuestXP) do
			if qid == tonumber(questID) then
				_xp = xp
			end
		end
		if _xp ~= nil then
			GameTooltip:SetOwner(QuestLogFrame, "ANCHOR_CURSOR")
			--GameTooltip:SetPoint("TOPRIGHT")
			GameTooltip:AddDoubleLine(title, tostring(_xp..' XP'), 1, 1, 1, 1, 1, 1)
			GameTooltip:Show()

			--local t = QuestLogQuestDescription:GetText()
			--QuestLogQuestDescription:SetText(t..'\n\nXP '.._xp)
		else
			GameTooltip:Hide()
		end
	end
end

--new event handling method using metatable (new to them)
AJ.EventsHandler = {}
AJ.EventsMeta = {}
setmetatable(AJ.EventsHandler, AJ.EventsMeta)
AJ.EventsMeta.__index = {
	--[==[
	["CHAT_MSG_ADDON"] = function(args)
		local data, i = {}, 1
		for d in string.gmatch(args.Message, '[^:]+') do
			data[i] = d
			i = i + 1
		end
		AJ.AddQuestXpData(data[1], data[2])
	end,
	]==]--
	["UI_INFO_MESSAGE"] = function(args)
		AJ.AddNote('discovered', tostring(args.ZoneName), nil)
		AJ.RefreshNotesPage()
	end,
	["PLAYER_DEAD"] = function()
		local deaths = AJ.GetCharacterstat('Character Deaths')
		AJ.UpdateCharacterstat('Character Deaths', deaths + 1)
		AJ.RefreshCharacterStatsUI()
	end,
	['LOOT_OPENED'] = function(args)
		AJ.ScanLootWindow()
	end,
	["QUEST_ACCEPTED"] = function(args)
		if AJ2_GlobalVariables[UnitGUID('player')].Notes == nil then
			AJ2_GlobalVariables[UnitGUID('player')].Notes = {}
		end
		AJ.AddNote('quest', tostring('Quest Accepted: '..args.QuestName), args.QuestID)
		AJ.RefreshNotesPage()
		local quests = AJ.GetCharacterstat('Quests Accepted')
		AJ.UpdateCharacterstat('Quests Accepted', quests + 1)
		AJ.RefreshCharacterStatsUI()
		local xp = 0
		xp = GetRewardXP()
		AJ.AddQuestXpData(args.QuestID, xp)
		--local xpDataSent = C_ChatInfo.SendAddonMessage("aj2-quest-XP", tostring(args.QuestID..':'..xp))
	end,
	["QUEST_TURNED_IN"] = function(args)
		local quest = GetQuestLogTitle(GetQuestLogIndexByID(args.QuestID))
		if quest then
			AJ.AddNote('quest-completed', tostring('Quest Completed: '..quest), args.QuestID)
		end
		AJ.RefreshNotesPage()
		local quests = AJ.GetCharacterstat('Quests Completed')
		AJ.UpdateCharacterstat('Quests Completed', quests + 1)
		AJ.RefreshCharacterStatsUI()
		local goldEarned = AJ.GetCharacterstat('Total Gold Earned')
		AJ.UpdateCharacterstat('Total Gold Earned', goldEarned + args.Money)
		AJ.RefreshCharacterStatsUI()
	end,
	["QUEST_REMOVED"] = function(args)
		local questsCompleted = GetQuestsCompleted()
		local questDone = false
		for questID, complete in pairs(questsCompleted) do
			if questID == args.QuestID and complete == true then
				questDone = true
			end
		end
		if questDone == false then
			local questsRemoved = AJ.GetCharacterstat('Quests Abandoned')
			AJ.UpdateCharacterstat('Quests Abandoned', questsRemoved + 1)
			AJ.RefreshCharacterStatsUI()
		end
	end,
	["UNIT_SPELLCAST_SENT"] = function(args)
		AJ.LastSpellTarget = args.Target
		local name, rank, icon, castTime, minRange, maxRange, spellId = GetSpellInfo(args.Spell)
		if name == 'Fishing' then
			AJ.FishingCast = true
		end
		if name == 'Food' then
			local foodConsumed = AJ.GetCharacterstat('Food Eaten')
			AJ.UpdateCharacterstat('Food Eaten', foodConsumed + 1)
			AJ.RefreshCharacterStatsUI()
		end
		if name == 'Drink' then
			local drinksConsumed = AJ.GetCharacterstat('Drinks Consumed')
			AJ.UpdateCharacterstat('Drinks Consumed', drinksConsumed + 1)
			AJ.RefreshCharacterStatsUI()
		end
		if name == 'First Aid' then
			local bandages = AJ.GetCharacterstat('Bandages Used')
			AJ.UpdateCharacterstat('Bandages Used', bandages + 1)
			AJ.RefreshCharacterStatsUI()
		end
	end,
	["CHAT_MSG_MONEY"] = function(args)
		if IsInGroup() == true then
			local money, c, s, g = {}, 0, 0, 0
			for word in string.gmatch(tostring(args.Message), "%w+" ) do table.insert(money, word) end
			for k, v in pairs(money) do
				--print(k, v)
				if v == 'Copper' then
					c = money[k-1]
				elseif v == 'Silver' then
					s = money[k-1]
				elseif v == 'Gold' then
					g = money[k-1]
				end
			end
			local lootShare = tonumber(c + (s * 100) + (g * 10000))
			local lootedGold = AJ.GetCharacterstat('Total Gold Looted')
			AJ.UpdateCharacterstat('Total Gold Looted', lootShare + lootedGold)
			AJ.RefreshCharacterStatsUI()
		end
	end,
	["COMBAT_LOG_EVENT"] = function()
		local playerGUID = select(8, CombatLogGetCurrentEventInfo())
		local npcGUID, npcName = nil, nil 
		if UnitGUID('player') == playerGUID then
			npcName = select(5, CombatLogGetCurrentEventInfo())
			npcGUID = select(4, CombatLogGetCurrentEventInfo())
			--print('using new api returns', npcName, npcGUID)
		else
			npcName = select(9, CombatLogGetCurrentEventInfo())
			npcGUID = select(8, CombatLogGetCurrentEventInfo())
			--print('using old api returns', npcName, npcGUID)
		end
		--this needs to be managed and kept small
		--print(timestamp, subevent, _, sourceGUID, sourceName, sourceFlags, sourceRaidFlags, destGUID, destName, destFlags, destRaidFlags)
		table.insert( AJ.RecentCombatTargets, { GUID = npcGUID, Name = npcName} ) -- using a timer to clear after 30 seconds atm
		--print('added ', npcName, npcGUID, ' to recent targets')
	end,
}