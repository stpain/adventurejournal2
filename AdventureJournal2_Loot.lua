local name, AJ = ...

AJ.loot = {}
AJ.LootSearchTable = {}

function AdventureJournalLootFrameSearchInputBox_OnTextChanged()
	AJ.RefreshLootListView()
end

function AdventureJournalLootFrameResetSearchButton_OnClick()
	AdventureJournalLootFrameSearchInputBox:SetText("")
	LootListViewFrameScrollBar:SetValue(1)
	AJ.CLearLootListView()
end

function AdventureJournalLootFrameIgnoreJunkCheckBox_OnClick()
	if AJ2_GlobalVariables[UnitGUID('player')].Settings == nil then
		AJ2_GlobalVariables[UnitGUID('player')].Settings = {}
		AJ2_GlobalVariables[UnitGUID('player')].Settings.IgnoreJunkLoot = not AJ2_GlobalVariables[UnitGUID('player')].Settings.IgnoreJunkLoot
	else
		AJ2_GlobalVariables[UnitGUID('player')].Settings.IgnoreJunkLoot = not AJ2_GlobalVariables[UnitGUID('player')].Settings.IgnoreJunkLoot
	end        
	AJ.RefreshLootListView()
end

function AJ.CLearLootListView()
	for i = 1, 10 do
		AJ.LootListViewItems[i].ItemName:SetText('')
		AJ.LootListViewItems[i].ItemZone:SetText('')
		AJ.LootListViewItems[i].ItemSource:SetText('')
		AJ.LootListViewItems[i]:Hide()
	end
end

function AJ.DrawLootPage()

	local LootListViewFrame = CreateFrame("SCROLLFRAME", "AdventureJournalLootFrameContent", AdventureJournalLootFrame)
	LootListViewFrame:SetPoint("TOPLEFT", 26, -100)
	LootListViewFrame:SetSize(400, 405)
	LootListViewFrame:SetBackdropColor(0, 0, 0, 1)

	local LootListViewFrameScrollBar = CreateFrame("Slider", 'LootListViewFrameScrollBar', LootListViewFrame, "UIPanelScrollBarTemplate")
	LootListViewFrameScrollBar:SetPoint("TOPLEFT", LootListViewFrame, "TOPRIGHT", -24,-12) 
	LootListViewFrameScrollBar:SetPoint("BOTTOMLEFT", LootListViewFrame, "BOTTOMRIGHT", -24, 20)
	LootListViewFrameScrollBar:SetMinMaxValues(1, 10)
	LootListViewFrameScrollBar:SetValueStep(1.0)
	LootListViewFrameScrollBar.scrollStep = 1
	LootListViewFrameScrollBar:SetValue(1)
	LootListViewFrameScrollBar:SetWidth(16)
	LootListViewFrameScrollBar:SetScript('OnValueChanged', function(self, value)
		local v = math.ceil(value)
		local i = 1
		AJ.CLearLootListView()
		for k, l in ipairs(AJ.LootSearchTable) do			
			if k < ((v * 10) + 1) and k > ((v - 1) * 10) then
				AJ.LootListViewItems[i]:Show()
				AJ.LootListViewItems[i].ItemName:SetText(tostring('|cff0070DEItem: |r'..l.Link))
				AJ.LootListViewItems[i].ItemZone:SetText(tostring('|cffABD473Zone: |r'..l.MapZoneName))
				AJ.LootListViewItems[i].ItemSource:SetText(tostring('|cffFF7D0ASource: |r'..l.SourceName))
				AJ.LootListViewItems[i].lootTime = time(l.Date)
				AJ.LootListViewItems[i].itemName = l.Name
				i = i + 1
			end
		end
	end)

	AJ.LootListViewItems = {}
	for i = 1, 10 do		
		local f = CreateFrame("FRAME", tostring("LootListViewItem"..i), LootListViewFrame)
		f:SetPoint("TOPLEFT", 0, (((i - 1) * 40) * -1) + 5)
		f:SetSize(380, 47)
		f:SetBackdrop({ bgFile = "Interface/Tooltips/UI-Tooltip-Background", 
						--edgeFile = "Interface/Tooltips/UI-Tooltip-Border", 
						tile = true, tileSize = 16, edgeSize = 16, 
						insets = { left = 4, right = 4, top = 4, bottom = 4 }});
		f:SetBackdropColor(0, 0, 0, 0.6)
		f:Hide()
		f.lootTime = nil
		f.itemName = nil
		
		f.ItemName = f:CreateFontString(nil, "OVERLAY", "GameFontNormal_NoShadow")
		f.ItemName:SetPoint("TOPLEFT", 8, -6)
		f.ItemName:SetFont("Fonts\\FRIZQT__.TTF", 12)
		f.ItemName:SetTextColor(1,1,1,1)

		f.ItemZone = f:CreateFontString(nil, "OVERLAY", "GameFontNormal_NoShadow")
		f.ItemZone:SetPoint("TOPLEFT", 12, -20)
		f.ItemZone:SetFont("Fonts\\FRIZQT__.TTF", 10)
		f.ItemZone:SetTextColor(1,1,1,1)

		f.ItemSource = f:CreateFontString(nil, "OVERLAY", "GameFontNormal_NoShadow")
		f.ItemSource:SetPoint("TOPLEFT", 12, -30)
		f.ItemSource:SetFont("Fonts\\FRIZQT__.TTF", 10)
		f.ItemSource:SetTextColor(1,1,1,1)

		AJ.LootListViewItems[i] = f
	end
end

--
function AJ.RefreshLootListView()
	--if AJ.SearchInputBox:GetText() then
	if AdventureJournalLootFrameSearchInputBox:GetText() then
		AJ.LootSearchTable = {}
		AJ.CLearLootListView()
		local c = 0
        for k, loot in pairs(AJ2_GlobalVariables.Loot) do
            if AJ2_GlobalVariables[UnitGUID('player')].Settings.IgnoreJunkLoot == true then
                if string.find(string.lower(loot.Name), string.lower(AdventureJournalLootFrameSearchInputBox:GetText())) and loot.Rarity > 0 then
                    table.insert(AJ.LootSearchTable, loot)
                    c = c + 1
                end
            else
                if string.find(string.lower(loot.Name), string.lower(AdventureJournalLootFrameSearchInputBox:GetText())) then
                    table.insert(AJ.LootSearchTable, loot)
                    c = c + 1
                end
            end
            
		end
		if c > 0 then
			table.sort(AJ.LootSearchTable, function(a, b) return a.Name < b.Name end)
			LootListViewFrameScrollBar:SetMinMaxValues(1, math.ceil(c / 10))
			for i, l in pairs(AJ.LootSearchTable) do
				if AJ.LootListViewItems[i] then
				--print(k, c)
					if l.Link then AJ.LootListViewItems[i].ItemName:SetText(tostring('|cff0070DEItem: |r'..l.Link)) end
					if l.MapZoneName then AJ.LootListViewItems[i].ItemZone:SetText(tostring('|cffABD473Zone: |r'..l.MapZoneName)) end
					if l.SourceName then AJ.LootListViewItems[i].ItemSource:SetText(tostring('|cffFF7D0ASource: |r'..l.SourceName)) end
					if l.Name then AJ.LootListViewItems[i].itemName = l.Name end
					if l.Date then AJ.LootListViewItems[i].lootTime = time(l.Date) end
					AJ.LootListViewItems[i]:Show()
				end
			end
		end
		--AJ.SearchResultText:SetText('Found '..c..' items')
		AdventureJournalLootFrameSearchResultsText:SetText('Found '..c..' items')
	end
	--LootListViewFrameScrollBar:SetValue(1)
end

function AJ.ScanLootWindow()
	local mapID = C_Map.GetBestMapForUnit('player')
	if mapID ~= nil then
		local currentMapPosition = C_Map.GetPlayerMapPosition(mapID, 'player')
		local currentMapName = C_Map.GetMapInfo(mapID).name
		--scan loot window and get item data
		for i = 1, GetNumLootItems() do

			if (LootSlotHasItem(i)) and GetLootSlotType(i) == 2 then				
				if IsInGroup() == false then
					info = GetLootInfo()
					--AJ.GetArgs(info[1])
					local money, c, s, g = {}, 0, 0, 0
					for word in string.gmatch(info[1].item, "%w+" ) do table.insert(money, word) end
					for k, v in pairs(money) do
						--print(k, v)
						if v == 'Copper' then
							c = money[k-1]
						elseif v == 'Silver' then
							s = money[k-1]
						elseif v == 'Gold' then
							g = money[k-1]
						end
					end
					local lootMoney = tonumber(c + (s * 100) + (g * 10000))
					local lootedGold = AJ.GetCharacterstat('Total Gold Looted')
					AJ.UpdateCharacterstat('Total Gold Looted', lootMoney + lootedGold)
					AJ.RefreshCharacterStatsUI()
				end
			end

			if (LootSlotHasItem(i)) and GetLootSlotType(i) == 1 then
				
				local newNode = true
				local sourceName = 'Unknown'
				local l = GetLootSlotLink(i)
				local icon = 134400 --default as ? icon
				local ignoreGameObject = false
				local ignoreLoot = false

				if AJ.LastGameObjectTarget == nil then
					icon = select(10, GetItemInfo(l))
				elseif select(1, GetLootSourceInfo(i)) == AJ.LastGameObjectTarget then
					icon = AJ.LastGameObjectTargetIcon
				end

				local _name = select(1, GetItemInfo(l))
				local rarity = select(3, GetItemInfo(l))

				if AJ2_GlobalVariables[UnitGUID('player')].Settings.IgnoreJunkLoot == true and rarity == 0 then
					return
				end
				if 1 == 1 then
					local price = select(11, GetItemInfo(l))
					local typeID = select(12, GetItemInfo(l))
					local subTypeID = select(13, GetItemInfo(l))
					local sourceGUID, _ = GetLootSourceInfo(i)

					if string.find(sourceGUID, 'Creature') then -- is this locale specific ?
						--print('creature', sourceGUID)
						if AJ.RecentCombatTargets ~= nil then
							for k, t in pairs(AJ.RecentCombatTargets) do
								if t.GUID == sourceGUID then
									sourceName = t.Name
								end
							end
						end
					elseif string.find(sourceGUID, 'GameObject') then
						sourceName = AJ.LastSpellTarget
						if AJ.LastSpellTarget ~= nil and string.find(string.lower(AJ.LastSpellTarget), string.lower('chest')) then -- set chest icon
							icon = 132594
						end
						if AJ.FishingCast == true then
							--sourceName = 'Fishing'
							if IsFishingLoot() == true then --additional check after finding api, player may cast fishing as last spell but then loot a previously killed mob
								local fishCaught = AJ.GetCharacterstat('Fishing Items Caught')
								AJ.UpdateCharacterstat('Fishing Items Caught', fishCaught + 1)
								sourceName = 'Fishing'
							end
						end
						AJ.FishingCast = false
					end

					for k, obj in ipairs(AJ2_GlobalVariables.GameObjects) do
						if obj.MapZoneName == currentMapName and obj.SourceName == sourceName then
							local inRange, distance = AJ.IsNodeInRange(currentMapPosition.x, currentMapPosition.y, obj.MapZonePosX, obj.MapZonePosY, 0.25)
							if inRange == true then
								obj.Date = date("*t")
								ignoreGameObject = true
							end
						end
					end

					for k, loot in ipairs(AJ2_GlobalVariables.Loot) do
						if loot.MapZoneName == currentMapName and loot.SourceName == sourceName then
							local inRange, distance = AJ.IsNodeInRange(currentMapPosition.x, currentMapPosition.y, loot.MapZonePosX, loot.MapZonePosY, 0.25)
							if inRange == true then
								loot.Date = date("*t")
								if loot.Name == _name then
									ignoreLoot = true
								end
							end
						end
					end

					--consider if game objects need to be stored twice? GameObjects table is used to populate map icons, Loot table is used for the loot search - game object is only recorded once whereas all loot is recorded?
					if 1 == 1 then
						if string.find(sourceGUID, 'GameObject') and sourceName ~= nil then
							if ignoreGameObject == false then
								if currentMapPosition then
									table.insert(AJ2_GlobalVariables.GameObjects, { Name = _name, Link = l, IconID = icon, Price = price, Rarity = rarity, ItemTypeID = typeID, ItemSubTypeID = subTypeID, SourceGUID = sourceGUID, SourceName = sourceName, MapID = mapID, MapZoneName = currentMapName, MapZonePosX = currentMapPosition.x, MapZonePosY = currentMapPosition.y, Date = date("*t") } )
								else
									table.insert(AJ2_GlobalVariables.GameObjects, { Name = _name, Link = l, IconID = icon, Price = price, Rarity = rarity, ItemTypeID = typeID, ItemSubTypeID = subTypeID, SourceGUID = sourceGUID, SourceName = sourceName, MapID = mapID, MapZoneName = GetMinimapZoneText(), MapZonePosX = 'Unknown', MapZonePosY = 'Unkown', Date = date("*t") } )
								end
								AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapDisplayObjects[sourceName] = true
							end
						end
						if ignoreLoot == false then
							if currentMapPosition then
								table.insert(AJ2_GlobalVariables.Loot, { Name = _name, Link = l, IconID = icon, Price = price, Rarity = rarity, ItemTypeID = typeID, ItemSubTypeID = subTypeID, SourceGUID = sourceGUID, SourceName = sourceName, MapID = mapID, MapZoneName = currentMapName, MapZonePosX = currentMapPosition.x, MapZonePosY = currentMapPosition.y, Date = date("*t") } )
							else
								table.insert(AJ2_GlobalVariables.Loot, { Name = _name, Link = l, IconID = icon, Price = price, Rarity = rarity, ItemTypeID = typeID, ItemSubTypeID = subTypeID, SourceGUID = sourceGUID, SourceName = sourceName, MapID = mapID, MapZoneName = GetMinimapZoneText(), MapZonePosX = 'Unknown', MapZonePosY = 'Unkown', Date = date("*t") } )
							end
						end
					end
					AJ.LastGameObjectTarget = sourceGUID
					AJ.LastGameObjectTargetIcon = icon
				end
			end
		end
		AJ.LastGameObjectTarget = nil
		AJ.LastGameObjectTargetIcon = nil
		AJ.FishingCast = false
	end
	AJ.UpdateMaps()
	AJ.RefreshCharacterStatsUI()
end