local name, AJ = ...

AJ.NotesListView = {}

function AdventureJournalNotesFrame_OnShow()
	AJ.RefreshNotesPage()
end

function AdventureJournalNotesFramePreviousDay_OnClick()
	AdventureJournalNotesFrameDateText:SetText(AJ.GetDateFormatted())
	AJ.NotesDateDisplayed = date("*t", time(AJ.NotesDateDisplayed) - (24*60*60))
	AdventureJournalNotesFrameDateText:SetText(AJ.GetDateFormatted(AJ.NotesDateDisplayed))
	AJ.RefreshNotesPage()
end

function AdventureJournalNotesFrameNextDay_OnClick()
	AdventureJournalNotesFrameDateText:SetText(AJ.GetDateFormatted())
	AJ.NotesDateDisplayed = date("*t", time(AJ.NotesDateDisplayed) + (24*60*60))
	AdventureJournalNotesFrameDateText:SetText(AJ.GetDateFormatted(AJ.NotesDateDisplayed))
	AJ.RefreshNotesPage()
end
--[==[
function AdventureJournalNotesFrameAddNoteButton_OnClick()
	if string.len(AJ.AddNoteInputBox:GetText()) > 0 then
		if AJ2_GlobalVariables[UnitGUID('player')].Notes == nil then
			AJ2_GlobalVariables[UnitGUID('player')].Notes = {}
		end
		if AJ2_GlobalVariables ~= nil and AJ2_GlobalVariables[UnitGUID('player')].Notes ~= nil then
			AJ.AddNote('note', AJ.AddNoteInputBox:GetText(), nil)
			AJ.AddNoteInputBox:SetText('')
			AJ.AddNoteInputBox:ClearFocus()
		end
	end
end
]==]--
function AdventureJournalNotesFrameAddNoteButton_OnClick()
	print('add note', AdventureJournalNotesFrameNoteInputBox:GetText())
	if string.len(AdventureJournalNotesFrameNoteInputBox:GetText()) > 0 then
		if AJ2_GlobalVariables[UnitGUID('player')].Notes == nil then
			AJ2_GlobalVariables[UnitGUID('player')].Notes = {}
		end
		if AJ2_GlobalVariables ~= nil and AJ2_GlobalVariables[UnitGUID('player')].Notes ~= nil then
			AJ.AddNote('note', AdventureJournalNotesFrameNoteInputBox:GetText(), nil)
			AdventureJournalNotesFrameNoteInputBox:SetText('')
			AdventureJournalNotesFrameNoteInputBox:ClearFocus()
		end
	end
end

function AJ.ClearNotesListView()
	for i = 1, 10 do
		if AJ.NotesListView[i] then
			AJ.NotesListView[i].NoteText:SetText('')
			AJ.NotesListView[i].NoteTime:SetText('')
			AJ.NotesListView[i].NoteText:SetTextColor(1, 1, 1, 1)
			AJ.NotesListView[i]:Hide()
		end
	end
end


AJ.AddNoteInputBox = nil
function AJ.DrawNotesPage()

	AJ.NotesDateDisplayed = date("*t")

	local NotesListViewFrame = CreateFrame("SCROLLFRAME", "AdventureJournalNotesFrameContent", AdventureJournalNotesFrame)
	NotesListViewFrame:SetPoint("TOPLEFT", 26, -100)
	NotesListViewFrame:SetSize(400, 405)

	local NotesListViewFrameScrollBar = CreateFrame("Slider", 'NotesListViewFrameScrollBar', NotesListViewFrame, "UIPanelScrollBarTemplate")
	NotesListViewFrameScrollBar:SetPoint("TOPLEFT", NotesListViewFrame, "TOPRIGHT", -24,-12) 
	NotesListViewFrameScrollBar:SetPoint("BOTTOMLEFT", NotesListViewFrame, "BOTTOMRIGHT", -24, 20)
	NotesListViewFrameScrollBar:SetMinMaxValues(1, 10)
	NotesListViewFrameScrollBar:SetValueStep(1.0)
	NotesListViewFrameScrollBar.scrollStep = 1
	NotesListViewFrameScrollBar:SetValue(1)
	NotesListViewFrameScrollBar:SetWidth(16)
	NotesListViewFrameScrollBar:SetScript('OnValueChanged', function(self, value)
        AJ.RefreshNotesPage()
	end)


	for i = 1, 10 do		
		local f = CreateFrame("FRAME", tostring("NotesListViewItem"..i), NotesListViewFrame)
		f:SetPoint("TOPLEFT", 0, (((i - 1) * 40) * -1) + 5)
		f:SetSize(380, 45)
		f:SetBackdrop({ bgFile = "Interface/Tooltips/UI-Tooltip-Background", 
						--edgeFile = "Interface/Tooltips/UI-Tooltip-Border", 
						tile = true, tileSize = 16, edgeSize = 16, 
						insets = { left = 4, right = 4, top = 4, bottom = 4 }});
		f:SetBackdropColor(0, 0, 0, 0.05)

		f.NoteTime = f:CreateFontString(nil, "OVERLAY")
		f.NoteTime:SetPoint("TOPLEFT", 8, -4)
		f.NoteTime:SetSize(60, 38)
		f.NoteTime:SetFont("Fonts\\MORPHEUS.ttf", 14)
		f.NoteTime:SetTextColor(0,0,0,1)
		f.NoteTime:SetJustifyH('left')
		f.NoteTime:SetJustifyV('center')

		f.NoteText = f:CreateFontString(nil, "OVERLAY")--, "GameFontNormal_NoShadow")
		f.NoteText:SetPoint("TOPLEFT", 70, -4)
		f.NoteText:SetSize(300, 38)
		f.NoteText:SetFont("Fonts\\MORPHEUS.ttf", 14)
		f.NoteText:SetTextColor(0.13, 0.08, 0.03, 1)
		f.NoteText:SetJustifyH('left')
        f.NoteText:SetJustifyV('middle')

        f.Note = ''

        f:SetScript('OnEnter', function() AJ.NoteTooltip(f, f.Note) end)
        f:SetScript('OnLeave', function() GameTooltipTextLeft1:SetFont("Fonts\\FRIZQT__.TTF", 14) GameTooltip_SetDefaultAnchor(GameTooltip, UIParent) end)
        
        AJ.NotesListView[i] = f
	end
end

function AJ.RefreshNotesPage()
	AdventureJournalNotesFrameDateText:SetText(AJ.GetDateFormatted(AJ.NotesDateDisplayed))
	if AJ2_GlobalVariables == nil then
		return
	end
	if AJ2_GlobalVariables[UnitGUID('player')] == nil then
		return
	end
	
	AJ.ClearNotesListView()
	local notesSorted = {}
    local today = date("*t", time(AJ.NotesDateDisplayed))
    --print(today.day)
	local c = 0
	if AJ2_GlobalVariables[UnitGUID('player')].Notes == nil then
		AJ2_GlobalVariables[UnitGUID('player')].Notes = {}
	end
	if AJ2_GlobalVariables ~= nil and AJ2_GlobalVariables[UnitGUID('player')].Notes ~= nil then
		for k, note in pairs(AJ2_GlobalVariables[UnitGUID('player')].Notes) do			
			if note.Note and note.Date.year == today.year and note.Date.month == today.month and note.Date.day == today.day then
				table.insert(notesSorted, note)
				c = c + 1
				--print(note.Note, note.Date.hour, note.Date.min)
			end
		end
	end		
	
	if c > 0 then
		table.sort(notesSorted, function(a, b) return time(a.Date) < time(b.Date) end)
		NotesListViewFrameScrollBar:SetMinMaxValues(1, math.ceil(c / 10))

		local v = math.ceil(NotesListViewFrameScrollBar:GetValue())
		local i = 1

		for k, n in ipairs(notesSorted) do
			--if n.Note then
				if k < ((v * 10) + 1) and k > ((v - 1) * 10) then
					AJ.NotesListView[i]:Show()
					AJ.NotesListView[i].NoteTime:SetText(tostring(date("%H:%M:%S", time(n.Date))..':'))
					--AJ.NotesListView[i].NoteText:SetText(tostring(AJ.NotesItemTitle[n.Type]..n.Note))
					AJ.NotesListView[i].NoteText:SetText(tostring(n.Note)) 
					for j, note in ipairs(AJ2_GlobalVariables[UnitGUID('player')].Settings.NoteColours) do
						if note.NoteID == n.Type then
							AJ.NotesListView[i].NoteText:SetTextColor(note.RGB.r, note.RGB.g, note.RGB.b, 1)
						end
					end
					AJ.NotesListView[i].Note = tostring(n.Note) -- n.Note
					i = i + 1
				end
			--end
		end
	end
end

function AJ.CreateNoteTooltipText(note)
    if note.Type == 'quest' then
        return tostring('Quest Accepted')
    elseif note.Type == 'quest-complete' then
        return tostring('Quest completed')
    elseif note.Type == 'discovery' then
        return tostring('Discovered')
    elseif note.Type == 'note' then
        return tostring()
    end
end

function AJ.AddNote(noteType, noteText, questID)
	if AJ2_GlobalVariables[UnitGUID('player')] ~= nil then
		if AJ2_GlobalVariables[UnitGUID('player')].Notes ~= nil then
			--get map data
			local map = C_Map.GetBestMapForUnit('player')
			if map then
				local currentMapPosition = C_Map.GetPlayerMapPosition(map, 'player')
				local currentMapName = C_Map.GetMapInfo(map).name
				local xp = 0
				if noteType == 'quest' then
					xp = GetRewardXP()
					if xp == nil then
						xp = 0
					end
				end
				if currentMapPosition then
					table.insert(AJ2_GlobalVariables[UnitGUID('player')].Notes, { XP = xp, Type = noteType, Note = noteText, Date = date("*t"), QuestID = questID, MapZoneName = currentMapName, MapZonePosX = currentMapPosition.x, MapZonePosY = currentMapPosition.y })
				else
					table.insert(AJ2_GlobalVariables[UnitGUID('player')].Notes, { XP = xp, Type = noteType, Note = noteText, Date = date("*t"), QuestID = questID, MapZoneName = 'unknown', MapZonePosX = 'unknown', MapZonePosY = 'unknown' })
				end
			end
		end
		AJ.RefreshNotesPage()
	end
end

function AJ.AppendNewNoteData(dropdownText, noteID)
	if AJ2_GlobalVariables[UnitGUID('player')].Settings.NoteColours ~= nil then
		local noteExists = false
		for k, n in ipairs(AJ2_GlobalVariables[UnitGUID('player')].Settings.NoteColours) do
			if n.NoteID == noteID then
				noteExists = true
			end
		end
		if noteExists == false then
			table.insert(AJ2_GlobalVariables[UnitGUID('player')].Settings.NoteColours, { DropdownText = dropdownText, NoteID = noteID, RGB = { r = 1, g = 1, b = 1, } })
		end
	end
end


--header = type
--text1 = body
--doubleline = coords
function AJ.NoteTooltip(frame, text)
    GameTooltip:SetOwner(frame) --, "ANCHOR_CURSOR")
    GameTooltip:SetPoint("TOPLEFT")
    --GameTooltipTextLeft1:SetFont("Fonts\\MORPHEUS.ttf", 13)
    if frame then
        GameTooltip:AddLine(text, 1, 1, 1, 1, 1)
        GameTooltip:Show()
    end
end