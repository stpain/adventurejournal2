local name, AJ = ...

StaticPopupDialogs["AdventureJournal_PurgeLootDialog"] = {
	text = "Remove all loot from your journal?.",
	button1 = "Ok",
	button2 = 'Cancel',
	OnAccept = function(self) AJ2_GlobalVariables.Loot = {} AJ.RefreshLootListView() AJ.Print('all loot removed from journal') end,
	timeout = 0,
	whileDead = true,
	hideOnEscape = true,
	preferredIndex = 3,
}

--need to fix this
StaticPopupDialogs["AdventureJournal_ResetCharacter"] = {
	text = "Reset data for %s",
	button1 = "Ok",
	button2 = 'Cancel',
	OnAccept = function(self) 
		AJ2_GlobalVariables[UnitGUID('player')] = {}
		if AJ2_GlobalVariables[UnitGUID('player')].Settings == nil then
			AJ2_GlobalVariables[UnitGUID('player')].Settings = {
				Name = UnitName('player'),
				IgnoreJunkLoot = true, 
				MinimapIconSize = 8.0, 
				HideMinimapPinDistance = 2.5,
				UseMinimapDistanceTracking = false,
				DisplayMinimapIcons = true, 
				DisplayWorldMapIcons = false,
				GuildShare = false, 
				MinimapDisplayObjects = {}, 
				WorldMapDisplayObjects = {},
				NoteColours = AJ.NoteColours,
				TooltipLines = AJ.TooltipLines,
				NoteTracking = AJ.NoteTracking,
				CharacterStatistics = AJ.CharacterStatistics,
			}
		end	
		if AJ2_GlobalVariables[UnitGUID('player')].Notes == nil then
			AJ2_GlobalVariables[UnitGUID('player')].Notes = {}
		end
		AJ.Print('character data removed and set to default.')
	end,
	timeout = 0,
	whileDead = true,
	hideOnEscape = true,
	preferredIndex = 3,
}

-- not used yet
StaticPopupDialogs["AdventureJournal_FixLootDb"] = {
	text = "Attempt to fix any errors in the journals loot database?.",
	button1 = "Ok",
	button2 = 'Cancel',
	OnAccept = function(self) 
		local lc, goc = 0, 0
		for k, loot in pairs(AJ2_GlobalVariables.Loot) do
			if loot.SourceName == nil then loot.SourceName = 'Unknown' lc = lc + 1 end
		end
		for k, loot in pairs(AJ2_GlobalVariables.GameObjects) do
			if loot.SourceName == nil then loot.SourceName = 'Unknown' goc = goc + 1 end
		end
		print('fixed', lc, 'loot entries,', goc, 'game object entries')
	end,
	timeout = 0,
	whileDead = true,
	hideOnEscape = true,
	preferredIndex = 3,
}