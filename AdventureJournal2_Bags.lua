local name, AJ = ...

AJ.BagFrameListViewItems = {}

function AJ.CLearBagListView()
	for i = 1, 20 do
		if AJ.BagFrameListViewItems[i] then
			AJ.BagFrameListViewItems[i].ItemName:SetText('')
			AJ.BagFrameListViewItems[i].ItemType:SetText('')
			AJ.BagFrameListViewItems[i].ItemCount:SetText('')
			AJ.BagFrameListViewItems[i]:Hide()
		end
	end
end

function AJ.DrawBagsFrame()
	AJ.BagsConsumablesListView = CreateFrame("SCROLLFRAME", "AJ_ConsumablesListViewFrame", AdventureJournalBagsFrame)
	AJ.BagsConsumablesListView:SetPoint("TOPLEFT", 26, -100)
	AJ.BagsConsumablesListView:SetSize(400, 400)
	AJ.BagsConsumablesListView.texture = AJ.BagsConsumablesListView:CreateTexture(nil, "ARTWORK")
	AJ.BagsConsumablesListView.texture:SetAllPoints(AJ.BagsConsumablesListView)
	AJ.BagsConsumablesListView.texture:SetTexture(0,0,0,1)

	AJ.BagsConsumablesListView.scrollFrame = CreateFrame("Slider", "AJ_ConsumablesListViewFrame_ScrollFrame", AJ.BagsConsumablesListView, "UIPanelScrollBarTemplate")
	AJ.BagsConsumablesListView.scrollFrame:SetPoint("TOPLEFT", AJ.BagsConsumablesListView, "TOPRIGHT", -24,-12) 
	AJ.BagsConsumablesListView.scrollFrame:SetPoint("BOTTOMLEFT", AJ.BagsConsumablesListView, "BOTTOMRIGHT", -24, 20)
	AJ.BagsConsumablesListView.scrollFrame:SetMinMaxValues(1, 20)
	AJ.BagsConsumablesListView.scrollFrame:SetValueStep(1.0)
	AJ.BagsConsumablesListView.scrollFrame.scrollStep = 1
	AJ.BagsConsumablesListView.scrollFrame:SetValue(1)
	AJ.BagsConsumablesListView.scrollFrame:SetWidth(16)
	AJ.BagsConsumablesListView.scrollFrame:SetScript('OnValueChanged', function(self, value)
		local v = math.ceil(value)
		local i = 1
		AJ.CLearBagListView()
		for k, l in ipairs(AJ.BagsItemList) do			
			if k < ((v * 20) + 1) and k > ((v - 1) * 20) then
				AJ.BagFrameListViewItems[i]:Show()
				AJ.BagFrameListViewItems[i].ItemName:SetText(l.Link)
				AJ.BagFrameListViewItems[i].ItemType:SetText(select(6, GetItemInfo(l.Link)))
				if l.TypeID == 0 or l.TypeID == 7 then
					AJ.BagFrameListViewItems[i].ItemType:SetText(tostring(AJ.BagsItemTypeColours[l.TypeID]..select(6, GetItemInfo(l.Link))))
				end
				AJ.BagFrameListViewItems[i].ItemCount:SetText(l.SlotCount)
				i = i + 1
			end
		end
	end)

	for i = 1, 20 do		
		local f = CreateFrame("FRAME", tostring("BagsListViewItem"..i), AJ.BagsConsumablesListView)
		f:SetPoint("TOPLEFT", 0, (((i - 1) * 20) * -1) + 5)
		f:SetSize(380, 26)
		f:SetBackdrop({ bgFile = "Interface/Tooltips/UI-Tooltip-Background", 
						--edgeFile = "Interface/Tooltips/UI-Tooltip-Border", 
						tile = true, tileSize = 16, edgeSize = 16, 
						insets = { left = 4, right = 4, top = 4, bottom = 4 }});
		f:SetBackdropColor(0, 0, 0, 0.6)
	
		f.ItemName = f:CreateFontString(nil, "OVERLAY", "GameFontNormal_NoShadow")
		f.ItemName:SetPoint("LEFT", 8, 0)
		f.ItemName:SetFont("Fonts\\FRIZQT__.TTF", 12)
		f.ItemName:SetTextColor(1,1,1,1)
		f.ItemName:SetText('test item name')

		f.ItemType = f:CreateFontString(nil, "OVERLAY", "GameFontNormal_NoShadow")
		f.ItemType:SetPoint("LEFT", 230, 0)
		f.ItemType:SetFont("Fonts\\FRIZQT__.TTF", 12)
		f.ItemType:SetTextColor(1,1,1,1)
		f.ItemType:SetText('test type')

		f.ItemCount = f:CreateFontString(nil, "OVERLAY", "GameFontNormal_NoShadow")
		f.ItemCount:SetPoint("LEFT", 340, 0)
		f.ItemCount:SetFont("Fonts\\FRIZQT__.TTF", 12)
		f.ItemCount:SetTextColor(1,1,1,1)
		f.ItemCount:SetText('20')

		AJ.BagFrameListViewItems[i] = f
	end
end

function AJ.RefreshBagsListView()
	if AJ.BagsConsumablesListView ~= nil and AJ.BagFrameListViewItems ~= nil then
		AJ.CLearBagListView()
		AJ.BagsItemList = {}
		local c = 0
		for bag = 0, 4 do
			for slot = 1, GetContainerNumSlots(bag) do
				local link = GetContainerItemLink(bag,slot)
				if link then
					local rarity = select(3, GetItemInfo(link))
					local vendorPrice = select(11, GetItemInfo(link))
					local typeID = select(12, GetItemInfo(link))
					local subTypeID = select(13, GetItemInfo(link))
					local icon = select(10, GetItemInfo(link))
					local count = select(2, GetContainerItemInfo(bag, slot))

					table.insert(AJ.BagsItemList, { BagId = bag, SlotID = slot, SlotCount = count, Link = link, Rarity = rarity, VendorPrice = vendorPrice, TypeID = typeID, SubTypeID = subTypeID, Icon = icon })
					c = c + 1
				end
			end
		end

		table.sort(AJ.BagsItemList, function(a,b) return a.TypeID < b.TypeID end)

		for k, i in ipairs(AJ.BagsItemList) do
			if AJ.BagFrameListViewItems[k] then
				AJ.BagFrameListViewItems[k].ItemName:SetText(i.Link)
				AJ.BagFrameListViewItems[k].ItemType:SetText(select(6, GetItemInfo(i.Link)))
				if i.TypeID == 0 or i.TypeID == 7 then
					AJ.BagFrameListViewItems[k].ItemType:SetText(tostring(AJ.BagsItemTypeColours[i.TypeID]..select(6, GetItemInfo(i.Link))))
				end
				AJ.BagFrameListViewItems[k].ItemCount:SetText(i.SlotCount)
				AJ.BagFrameListViewItems[k]:Show()
			end
		end
		AJ.BagsConsumablesListView.scrollFrame:SetValue(1)
		if c > 20 then
			AJ.BagsConsumablesListView.scrollFrame:SetMinMaxValues(1, c/20)
		end
	end
end


AJ.BagsItemTypeColours = { [7] = '|cff40C7EB', [0] = '|cffABD473' }

function AJ.SellJunk()
	if MerchantFrame:IsVisible() then
		local sold_value = 0
		local sold_items = 0
		local sales = {}
		for bag = 0, 4 do
			for slot = 1, GetContainerNumSlots(bag) do 
				local item_link = GetContainerItemLink(bag,slot) 
				if item_link and select(3, GetItemInfo(item_link)) == 0 then
					-- get item data
					item_count = select(2, GetContainerItemInfo(bag, slot))
					sold_items = sold_items + item_count
					sold_value = sold_value + (item_count * select(11, GetItemInfo(item_link)))
					--update local table
					if sales == nil then
						table.insert(sales, { link = item_link, count = item_count, cost = select(11, GetItemInfo(item_link)) })
					else
						local added = false
						for k, item in ipairs(sales) do
							if item.link == item_link then 
								item.count = item.count + item_count
								added = true
							end
						end
						if added == false then
							table.insert(sales, { link = item_link, count = item_count, cost = select(11, GetItemInfo(item_link)) })
						end
					end					
					-- sell item
					UseContainerItem(bag,slot)
				end
			end
		end	
		-- print sell info check
		if 1 == 1 then
			for k, item in ipairs(sales) do
				AJ.PrintMoney('sold '..item.count..' '..item.link..' for ', tonumber(item.count * item.cost))
			end
		end
		AJ.PrintMoney(tostring('sold '..tostring(sold_items)..' for '), tonumber(sold_value))
	end
end

-- sell white equipment to vendor
function AJ.SellWhiteEquipment()
	local sold_value = 0
	local sold_items = 0
	local sell_item = false
	local sales = {}
	for bag = 0, 4 do
		for slot = 1, GetContainerNumSlots(bag) do 
			local item_link = GetContainerItemLink(bag,slot) 
			if item_link and select(3, GetItemInfo(item_link)) == 1 then				
				-- ignore tabards
				if string.find(select(1, GetItemInfo(item_link)), 'Tabard') then
					AJ.Print('found '..item_link..' item not sold.')
				else
					if select(6, GetItemInfo(item_link)) == 'Weapon' then
						if string.find(select(7, GetItemInfo(item_link)), "Fishing") then
							AJ.Print('found '..item_link..' item not sold.')
						elseif select(7, GetItemInfo(item_link)) == 'Miscellaneous' then
							AJ.Print('...strange item found, will not be sold.')
						else
							sell_item = true
						end
					elseif select(6, GetItemInfo(item_link)) == 'Armor' then
						sell_item = true
					end
				end
				if sell_item == true then
					item_count = select(2, GetContainerItemInfo(bag, slot))
					sold_items = sold_items + item_count
					sold_value = sold_value + (item_count * select(11, GetItemInfo(item_link)))
					if sales == nil then
						table.insert(sales, { link = item_link, count = 1, cost = select(11, GetItemInfo(item_link)) })
					else
						local added = false
						for k, item in ipairs(sales) do
							if item.link == item_link then
								item.count = item.count + 1
								added = true
							end
						end
						if added == false then
							table.insert(sales, { link = item_link, count = 1, cost = select(11, GetItemInfo(item_link)) })
						end
					end					
					UseContainerItem(bag,slot)					
					sell_item = false
				end
			end
		end
	end
	if 1 == 1 then
		for k, item in ipairs(sales) do
			AJ.PrintMoney('sold '..item.count..' '..item.link..' for ', tonumber(item.count * item.cost))
		end
	end
	AJ.PrintMoney(tostring('sold '..tostring(sold_items)..' items for '), tonumber(sold_value))
end

-- sell green equipment to vendor
function AJ.SellGreenEquipemnt()
	local sold_value = 0
	local sold_items = 0
	local sell_item = false
	local sales = {}
	for bag = 0, 4 do
		for slot = 1, GetContainerNumSlots(bag) do 
			local item_link = GetContainerItemLink(bag,slot) 
			if item_link and select(3, GetItemInfo(item_link)) == 2 then
				if select(6, GetItemInfo(item_link)) == 'Weapon' then
					if select(7, GetItemInfo(item_link)) == 'Fishing Poles' then
						AJ.Print('AJ2, found '..item_link..' item not sold.')
					elseif select(7, GetItemInfo(item_link)) == 'Miscellaneous' then
						AJ.Print('AJ2, ...strange item found, will not be sold.')
					else
						sell_item = true
					end
				elseif select(6, GetItemInfo(item_link)) == 'Armor' then
					sell_item = true
				end
				if sell_item == true then					
					item_count = select(2, GetContainerItemInfo(bag, slot))
					sold_items = sold_items + item_count
					sold_value = sold_value + (item_count * select(11, GetItemInfo(item_link)))
					if sales == nil then
						table.insert(sales, { link = item_link, count = 1, cost = select(11, GetItemInfo(item_link)) })
					else
						local added = false
						for k, item in ipairs(sales) do
							if item.link == item_link then
								item.count = item.count + 1
								added = true
							end
						end
						if added == false then
							table.insert(sales, { link = item_link, count = 1, cost = select(11, GetItemInfo(item_link)) })
						end
					end					
					UseContainerItem(bag,slot)					
					sell_item = false
				end
			end
		end
	end
	if 1 == 1 then
		for k, item in ipairs(sales) do
			AJ.PrintMoney('sold '..item.count..' '..item.link..' for ', tonumber(item.count * item.cost))
		end
	end
	AJ.PrintMoney(tostring('sold '..tostring(sold_items)..' items for '), tonumber(sold_value))
end

AJ.MerchantExtensionConfig = { 
	Button = {
		Width = 100,
		Height = 22,
	}
}

AJ.MerchantExtensionFrame = CreateFrame('Frame', 'AJMerchantFrame', MerchantFrame)
AJ.MakeFrameMove(AJ.MerchantExtensionFrame)
AJ.MerchantExtensionFrame:SetPoint('TOPLEFT', MerchantFrame:GetWidth(), -20)
AJ.MerchantExtensionFrame:SetSize(175, 150)
AJ.MerchantExtensionFrame:SetBackdrop({ --bgFile = "Interface/Tooltips/UI-Tooltip-Background", 
									edgeFile = "Interface/Tooltips/UI-Tooltip-Border", 
									tile = true, tileSize = 16, edgeSize = 20, 
									insets = { left = 4, right = 4, top = 4, bottom = 4 }})
AJ.MerchantExtensionFrame.Texture = AJ.MerchantExtensionFrame:CreateTexture('$parent_Texture', 'Background')
--AJ.MerchantExtensionFrame.Texture:SetAllPoints(AJ.MerchantExtensionFrame)
AJ.MerchantExtensionFrame.Texture:SetTexture(136267)
AJ.MerchantExtensionFrame.Texture:SetPoint('TOPLEFT', 4, -4)
AJ.MerchantExtensionFrame.Texture:SetPoint('BOTTOMRIGHT', -4, 4)

--title
AJ.MerchantExtensionFrame.Title = AJ.MerchantExtensionFrame:CreateFontString('AJMerchantFrameTitle', 'OVERLAY', 'GameFontNormal')
AJ.MerchantExtensionFrame.Title:SetText('Adventure Journal 2')
AJ.MerchantExtensionFrame.Title:SetFont("Fonts\\MORPHEUS.ttf", 14)
AJ.MerchantExtensionFrame.Title:SetTextColor(1,1,1,1)
AJ.MerchantExtensionFrame.Title:SetPoint('TOP', 0, -8)

-- sell junk/grey items
AJ.MerchantExtensionFrame.SellJunkButton = CreateFrame("BUTTON", "AJSellJunkButton", AJ.MerchantExtensionFrame, "UIPanelButtonTemplate")
AJ.MerchantExtensionFrame.SellJunkButton:SetText('|cFFFFFFFFSell Junk|r')
AJ.MerchantExtensionFrame.SellJunkButton:SetPoint("TOP", 0, -30)
AJ.MerchantExtensionFrame.SellJunkButton:SetSize(AJ.MerchantExtensionConfig.Button.Width, AJ.MerchantExtensionConfig.Button.Height)
AJ.MerchantExtensionFrame.SellJunkButton:SetScript('OnClick', function() AJ.SellJunk() end)

-- header
AJ.MerchantExtensionFrame.EquipmentHeader = AJ.MerchantExtensionFrame:CreateFontString('AJMerchantFrameEquipmentHeader', 'OVERLAY', 'GameFontNormal')
AJ.MerchantExtensionFrame.EquipmentHeader:SetText('Equipment & Armor')
AJ.MerchantExtensionFrame.EquipmentHeader:SetFont("Fonts\\MORPHEUS.ttf", 14)
AJ.MerchantExtensionFrame.EquipmentHeader:SetTextColor(1,1,1,1)
AJ.MerchantExtensionFrame.EquipmentHeader:SetPoint('TOP', 0, -60)

-- vendor white equipment
AJ.MerchantExtensionFrame.SellWhiteEquipment = CreateFrame("BUTTON", "AJSellWhiteEquipment", AJ.MerchantExtensionFrame, "UIPanelButtonTemplate")
AJ.MerchantExtensionFrame.SellWhiteEquipment:SetText('|cFFFFFFFFWhite Items|r')
AJ.MerchantExtensionFrame.SellWhiteEquipment:SetPoint("TOP", 0, -85)
AJ.MerchantExtensionFrame.SellWhiteEquipment:SetSize(AJ.MerchantExtensionConfig.Button.Width, AJ.MerchantExtensionConfig.Button.Height)
AJ.MerchantExtensionFrame.SellWhiteEquipment:SetScript('OnClick', function() AJ.SellWhiteEquipment() end)

-- vendor green equipment
AJ.MerchantExtensionFrame.SellSoulboundGreens = CreateFrame("BUTTON", "AJSellWhiteEquipment", AJ.MerchantExtensionFrame, "UIPanelButtonTemplate")
AJ.MerchantExtensionFrame.SellSoulboundGreens:SetText('|cFFFFFFFFGreen Items|r')
AJ.MerchantExtensionFrame.SellSoulboundGreens:SetPoint("TOP", 0, -115)
AJ.MerchantExtensionFrame.SellSoulboundGreens:SetSize(AJ.MerchantExtensionConfig.Button.Width, AJ.MerchantExtensionConfig.Button.Height)
AJ.MerchantExtensionFrame.SellSoulboundGreens:SetScript('OnClick', function() AJ.SellGreenEquipemnt() end)

function AdventureJournalBagsFrame_OnShow()
	if AdventureJournalBagsFrame_PlayerGoldText then
		AdventureJournalBagsFrame_PlayerGoldText:SetText("Gold "..GetCoinTextureString(GetMoney()))
	end
	AJ.RefreshBagsListView()
end