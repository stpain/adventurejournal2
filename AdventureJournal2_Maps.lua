local name, AJ = ...

local HBD = LibStub("HereBeDragons-2.0")
local Pins = LibStub("HereBeDragons-Pins-2.0")

AJ.MapNodeCacheMinimap = {}
AJ.MapNodeCacheWorldmap = {}

function AJ.UpdateMaps()
	if AJ2_GlobalVariables == nil then
		return
	else
		if AJ2_GlobalVariables.GameObjects ~= nil then
			AJ.HideMinimapPins()
			AJ.UpdateMiniMapPins()

			AJ.HideWorldMapPins()
			AJ.UpdateWorldMapPins()
		end
	end
end

function AJ.IsNodeInRange(myPosX, myPosY, nodePosX, nodePosY, range)
	if type(nodePosX) == 'number' and type(nodePosY) == 'number' and type(myPosY) == 'number' and type(myPosX) == 'number' then
		local distance = (((myPosX - nodePosX)^2.0) + ((myPosY - nodePosY)^2.0)) ^ 0.5
		distance = distance * 100
		if distance < range then -- query this value ?
			return true, distance
		else
			return false
		end
	end
end

function AJ.IsNodeInRangeMinimap(myPosX, myPosY, nodePosX, nodePosY)
	if type(nodePosX) == 'number' then
		local distance = (((myPosX - nodePosX)^2.0) + ((myPosY - nodePosY)^2.0)) ^ 0.5
		distance = distance * 140
		local zoom = Minimap:GetZoom()
		if zoom == 5 then
			if distance < 1.8 then
				return true
			else
				return false
			end
		elseif zoom == 4 then
			if distance < 2.4 then
				return true
			else
				return false
			end
		elseif zoom == 3 then
			if distance < 3.0 then
				return true
			else
				return false
			end
		elseif zoom == 2 then
			if distance < 3.8 then
				return true
			else
				return false
			end
		elseif zoom == 1 then
			if distance < 5.0 then
				return true
			else
				return false
			end
		elseif zoom == 0 then
			if distance < 6.0 then
				return true
			else
				return false
			end
		end
	end
end

function AJ.HideMinimapPins()
    Pins:RemoveAllMinimapIcons("AdventureJournal")
end

function AJ.HideWorldMapPins()
    Pins:RemoveAllWorldMapIcons("AdventureJournal")
end

function AJ.DrawMapNodeIconMinimap(f, x, y, w, h, level, strata, icon)
	f:SetPoint("CENTER", x, y)
	f:SetSize(w, h)
	f:SetFrameLevel(level)
	f:SetFrameStrata(strata)
	f:EnableKeyboard(true)
	f:SetPropagateKeyboardInput(true)
	
	f.texture = f:CreateTexture(nil, 'ARTWORK')
	f.texture:SetAllPoints(f)
	f.texture:SetTexture(icon)
end


--going to re work this over time to really improve performance, get map data once in the onUpdate
-- instead of using onUpdate just use map change events
function AJ.UpdateWorldMapPins()

	if AJ2_GlobalVariables.GameObjects ~= nil and AJ2_GlobalVariables[UnitGUID('player')].Settings.DisplayWorldMapIcons == true then
		--print('showing world map pins')
		local key = 1
		AJ.NodeGUIDsCreated = {}

		for k, loot in pairs(AJ2_GlobalVariables.GameObjects) do

			if loot and loot.MapZoneName == C_Map.GetMapInfo(WorldMapFrame:GetMapID()).name then
				
				--print(loot.Name, loot.MapZoneName)

				-- all uses minimap objects this isnt an error
				if AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapDisplayObjects[loot.SourceName] and AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapDisplayObjects[loot.SourceName] == true then

					if AJ.NodeGUIDsCreated[loot.SourceGUID] == true then

						--print(loot.SourceName, 'exists already')
					else
						--print(loot.SourceName, 'new')
						
						local x, y, instance = HBD:GetWorldCoordinatesFromZone(loot.MapZonePosX, loot.MapZonePosY, loot.MapID)

						if AJ.MapNodeCacheWorldmap[key] == nil then
					
							AJ.NodeGUIDsCreated[loot.SourceGUID] = true

							--print('creating new node icon', loot.Name, key, string.format("%.4f",loot.MapZonePosX * 100), string.format("%.4f",loot.MapZonePosY * 100))

							local f = CreateFrame("FRAME", "AJ_Pin"..key, UIParent)
							f:SetSize(tonumber(AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapIconSize), tonumber(AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapIconSize))
							f.texture = f:CreateTexture(nil, 'ARTWORK')
							f.texture:SetAllPoints(f)
							f.texture:SetTexture(loot.IconID)
							f:SetScript('OnEnter', function()
								AJ.MinimapTooltip(WorldMapFrame, loot)
							end)
							f:SetScript('OnLeave', function() 
								GameTooltip_SetDefaultAnchor(GameTooltip, UIParent)
							end)

							AJ.MapNodeCacheWorldmap[key] = f

							--Pins:AddMinimapIconWorld("AdventureJournal", f, instance, x, y, false)
				
							Pins:AddWorldMapIconWorld("AdventureJournal", f, instance, x, y)

						else
							--print('found node', key, loot.Name, string.format("%.4f",loot.MapZonePosX * 100), string.format("%.4f",loot.MapZonePosY * 100))
							AJ.MapNodeCacheWorldmap[key].texture:SetTexture(loot.IconID)
							AJ.MapNodeCacheWorldmap[key]:SetScript('OnEnter', function()
								AJ.MinimapTooltip(WorldMapFrame, loot)
							end)
							AJ.MapNodeCacheWorldmap[key]:SetScript('OnLeave', function() 
								GameTooltip_SetDefaultAnchor(GameTooltip, UIParent)
							end)
							AJ.MapNodeCacheWorldmap[key]:SetSize(tonumber(AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapIconSize), tonumber(AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapIconSize))
							AJ.MapNodeCacheWorldmap[key]:Show()
							Pins:AddWorldMapIconWorld("AdventureJournal", AJ.MapNodeCacheWorldmap[key], instance, x, y)
							AJ.NodeGUIDsCreated[loot.SourceGUID] = true
						end
						key = key + 1
					end
				end
			end
		end
	else
		--print('hiding world map pins')
		AJ.HideWorldMapPins()
	end
end

--------------------------------------------------------------------------------------------------

--this is a massive perfrmance issue, potential help with a sinlge map data request however it may prove better to draw all frames rather than recycle
--will just use map zone change events instead of onUpdate
function AJ.UpdateMiniMapPins()
	if AJ2_GlobalVariables.GameObjects ~= nil and AJ2_GlobalVariables[UnitGUID('player')].Settings.DisplayMinimapIcons == true then

		AJ.CurrentMapID = C_Map.GetBestMapForUnit('player')
		if not AJ.CurrentMapID then
			return
		else
			local currentMapPosition = C_Map.GetPlayerMapPosition(AJ.CurrentMapID, 'player')
			--AJ.CurrentMapPosition = C_Map.GetPlayerMapPosition(AJ.CurrentMapID, 'player')
			local key = 1
			AJ.MinimapNodeGUIDsCreated = {}

			for k, loot in pairs(AJ2_GlobalVariables.GameObjects) do

				if loot and loot.MapZoneName == C_Map.GetMapInfo(AJ.CurrentMapID).name then
				
					--print(loot.Name, loot.MapZoneName)

					if AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapDisplayObjects[loot.SourceName] and AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapDisplayObjects[loot.SourceName] == true then

						if AJ.MinimapNodeGUIDsCreated[loot.SourceGUID] == true then

							--print(loot.SourceName, 'exists already')
						else
							--print(loot.SourceName, 'new')

							local lootPosX, lootPosY, instance = HBD:GetWorldCoordinatesFromZone(loot.MapZonePosX, loot.MapZonePosY, loot.MapID)
							--local myPosX, myPosY, instance = HBD:GetWorldCoordinatesFromZone(currentMapPosition.x, currentMapPosition.y, AJ.CurrentMapID)

							if AJ.MapNodeCacheMinimap[key] == nil then
					
								AJ.MinimapNodeGUIDsCreated[loot.SourceGUID] = true

								--print('creating new node icon', loot.Name, key, string.format("%.4f",loot.MapZonePosX * 100), string.format("%.4f",loot.MapZonePosY * 100))

								local f = CreateFrame("FRAME", "AJ_Pin"..key, UIParent)
								f:SetSize(tonumber(AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapIconSize), tonumber(AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapIconSize))
								f.texture = f:CreateTexture(nil, 'ARTWORK')
								f.texture:SetAllPoints(f)
								f.texture:SetTexture(loot.IconID)
								f.posX = loot.MapZonePosX
								f.posY = loot.MapZonePosY
								f.mapZoneName = C_Map.GetMapInfo(AJ.CurrentMapID).name
								f.nodeName = loot.SourceName
								--f.date = loot.Date

								AJ.MapNodeCacheMinimap[key] = f
								AJ.MapNodeCacheMinimap[key]:SetScript('OnEnter', function()
									AJ.MinimapTooltip(Minimap, loot)
								end)
								AJ.MapNodeCacheMinimap[key]:SetScript('OnLeave', function()
									--GameTooltipTextLeft1:SetFont("Fonts\\FRIZQT__.TTF", 14) 
									GameTooltip_SetDefaultAnchor(GameTooltip, UIParent)
								end)

								Pins:AddMinimapIconWorld("AdventureJournal", f, instance, lootPosX, lootPosY, false)

							else
								--print('found node', key, loot.Name, string.format("%.4f",loot.MapZonePosX * 100), string.format("%.4f",loot.MapZonePosY * 100))
								AJ.MapNodeCacheMinimap[key].texture:SetTexture(loot.IconID)
								AJ.MapNodeCacheMinimap[key]:SetSize(tonumber(AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapIconSize), tonumber(AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapIconSize))
								
								AJ.MapNodeCacheMinimap[key].posX = loot.MapZonePosX
								AJ.MapNodeCacheMinimap[key].posY = loot.MapZonePosY
								AJ.MapNodeCacheMinimap[key].mapZoneName = C_Map.GetMapInfo(AJ.CurrentMapID).name
								AJ.MapNodeCacheMinimap[key].nodeName = loot.SourceName
								--AJ.MapNodeCacheMinimap[key].date = loot.Date
								
								AJ.MapNodeCacheMinimap[key]:SetScript('OnEnter', function()
									AJ.MinimapTooltip(Minimap, loot)
								end)
								AJ.MapNodeCacheMinimap[key]:SetScript('OnLeave', function()
									--GameTooltipTextLeft1:SetFont("Fonts\\FRIZQT__.TTF", 14) 
									GameTooltip_SetDefaultAnchor(GameTooltip, UIParent)
								end)

								
								if AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapDisplayObjects[loot.SourceName] == false then
									AJ.MapNodeCacheMinimap[key]:Hide()
								else
									Pins:AddMinimapIconWorld("AdventureJournal", AJ.MapNodeCacheMinimap[key], instance, lootPosX, lootPosY, false)
								end
								AJ.MinimapNodeGUIDsCreated[loot.SourceGUID] = true
							end
							key = key + 1
						end
					else
						--AJ.HideMinimapPins()
					end
				else

				end
			end
		end
	else
	AJ.HideMinimapPins()
	end
end

function AJ.MinimapTooltip(frame, obj)
    GameTooltip:SetOwner(frame, "ANCHOR_CURSOR")
    --GameTooltip:SetPoint("BOTTOM")
	local dropTable, dropAdded = AJ.GetGameObjectDrops(obj), {}
	local _, class, _ = UnitClass('player')	
	if frame then
		--GameTooltip:AddLine(tostring(AJ.ClassColours[class].fs..'Adventure Journal'..'|r'), 1, 1, 1)
		--GameTooltip:AddDoubleLine(tostring(AJ.ClassColours[class].fs..'Adventure Journal'..'|r'), obj.SourceName, 1, 1, 1, 1, 1, 1)
		--GameTooltip:AddDoubleLine(tostring(AJ.ClassColours[class].fs..'Adventure Journal'..'|r'), AJ.GetDateFormatted(obj.Date), 1, 1, 1, 1, 1, 1)
		GameTooltip:AddDoubleLine(obj.SourceName, AJ.GetDateFormatted(obj.Date), 1, 1, 1, 1, 1, 1)
		--GameTooltip:AddDoubleLine(obj.SourceName, AJ.GetTimeFormatted(obj.Date), 1, 1, 1, 1, 1, 1)
		--GameTooltip:AddDoubleLine(AJ.GetDateFormatted(obj.Date), AJ.GetTimeFormatted(obj.Date), 1, 1, 1, 1, 1, 1)
		GameTooltip:AddDoubleLine(' ', AJ.GetTimeFormatted(obj.Date), 1, 1, 1, 1, 1, 1)
		
		for k, drop in ipairs(dropTable) do
			if not dropAdded[drop.Name] then				
				GameTooltip:AddLine(drop.Name, 1, 1, 1, 1)				
				dropAdded[drop.Name] = true
			end
		end
        GameTooltip:Show()
    end
end

-- should reset tooltip font to game normal NEEDS CONFIRMING
function AJ.ResetGameTooltip()
	--GameTooltipTextLeft1:SetFont("Fonts\\FRIZQT__.TTF", 14) GameTooltip_SetDefaultAnchor(GameTooltip, UIParent)
end