local name, AJ = ...

--options interface
AJ.OptionsPanel = CreateFrame("Frame", "AJOptionsPanel", UIParent);
AJ.OptionsPanel.name = "AdventureJournal";
InterfaceOptions_AddCategory(AJ.OptionsPanel);

--add a title
AJ.OptionsPanel.Title = AJ.OptionsPanel:CreateFontString("AJOptionsPanelTitle", "OVERLAY", "GameFontNormalLarge")
AJ.OptionsPanel.Title:SetPoint("TOPLEFT", 16, -16)
--AJ.OptionsPanel.Title:SetFont("Fonts\\FRIZQT__.TTF", 14)
AJ.OptionsPanel.Title:SetText("Adventure Journal")

--about
AJ.OptionsPanel.About = AJ.OptionsPanel:CreateFontString('AJOptionsPanelAbout', 'OVERLAY', 'GameFontNormal')
AJ.OptionsPanel.About:SetPoint('TOPLEFT', 16, -36)
AJ.OptionsPanel.About:SetSize(580, 50)
AJ.OptionsPanel.About:SetJustifyH('LEFT')
AJ.OptionsPanel.About:SetText('Adventure Journal configurations.')
AJ.OptionsPanel.About:SetTextColor(1,1,1,1)


local AdventureJournalOptionsPage_MapHeader = AJ.OptionsPanel:CreateFontString("AJ_OptionsPageHeader_Map", "OVERLAY", "GameFontNormal_NoShadow")
AdventureJournalOptionsPage_MapHeader:SetPoint("TOPLEFT", 38, -100)
AdventureJournalOptionsPage_MapHeader:SetText("Map Options")
AdventureJournalOptionsPage_MapHeader:SetFont("Fonts\\FRIZQT__.TTF", 14)
AdventureJournalOptionsPage_MapHeader:SetTextColor(1, 1, 1, 1)

AJ.ShowMinimapIconsCheckBox = CreateFrame("CheckButton", 'AJ_ShowMinimapIconsCheckBox', AJ.OptionsPanel, "ChatConfigCheckButtonTemplate")
AJ.ShowMinimapIconsCheckBox:SetPoint('TOPLEFT', 40, -120)
AJ_ShowMinimapIconsCheckBoxText:SetText('|cffffffffMinimap Icons|r')
AJ_ShowMinimapIconsCheckBoxText:SetSize(150, 30)
AJ.ShowMinimapIconsCheckBox:SetScript('OnClick', function(self)
    if AJ2_GlobalVariables[UnitGUID('player')].Settings == nil then
        AJ2_GlobalVariables[UnitGUID('player')].Settings = {}
        AJ2_GlobalVariables[UnitGUID('player')].Settings.DisplayMinimapIcons = self:GetChecked()
    else
        AJ2_GlobalVariables[UnitGUID('player')].Settings.DisplayMinimapIcons = self:GetChecked()
    end
    if AJ2_GlobalVariables[UnitGUID('player')].Settings.DisplayMinimapIcons == false then
        AJ.HideMinimapPins()
    else
        AJ.UpdateMaps()
    end
end)
AJ.ShowMinimapIconsCheckBox.tooltip = 'Display icons on the minimap.'

AJ.ShowIconsWorldMapCheckBox = CreateFrame("CheckButton", 'AJ_ShowIconsWorldMapCheckBox', AJ.OptionsPanel, "ChatConfigCheckButtonTemplate")
AJ.ShowIconsWorldMapCheckBox:SetPoint('TOPLEFT', 40, -145)
AJ_ShowIconsWorldMapCheckBoxText:SetText('|cffffffffWorld Map Icons|r')
AJ_ShowIconsWorldMapCheckBoxText:SetSize(150, 30)
AJ.ShowIconsWorldMapCheckBox:SetScript('OnClick', function(self) 
    if AJ2_GlobalVariables[UnitGUID('player')].Settings == nil then
        AJ2_GlobalVariables[UnitGUID('player')].Settings = {}
        AJ2_GlobalVariables[UnitGUID('player')].Settings.DisplayWorldMapIcons = self:GetChecked()
    else
        AJ2_GlobalVariables[UnitGUID('player')].Settings.DisplayWorldMapIcons = self:GetChecked()
    end
    if AJ2_GlobalVariables[UnitGUID('player')].Settings.DisplayWorldMapIcons == false then
        AJ.HideWorldMapPins()
    else
        AJ.UpdateMaps()
    end
end)
AJ.ShowIconsWorldMapCheckBox.tooltip = 'Display icons on the World Map. \n\nFor changes to take effect the world map needs to be shown.'


-- issue with drop downs is limited screen room -> will need to create level structures to keep this idea!
AJ.MinimapDisplayObjectDropdown = CreateFrame("FRAME", "AJ_MinimapDisplayObjectDropdown", AJ.OptionsPanel, "UIDropDownMenuTemplate")
AJ.MinimapDisplayObjectDropdown:SetPoint("TOPLEFT", 26, -175)
AJ.MinimapDisplayObjectDropdown.displayMode = "MENU"
UIDropDownMenu_SetWidth(AJ.MinimapDisplayObjectDropdown, 140)
UIDropDownMenu_SetText(AJ.MinimapDisplayObjectDropdown, 'Map Objects')

-- TOOLTIP
local AdventureJournalOptionsPage_TooltipHeader = AJ.OptionsPanel:CreateFontString("AJ_OptionsPageHeader_Tooltip", "OVERLAY", "GameFontNormal_NoShadow")
AdventureJournalOptionsPage_TooltipHeader:SetPoint("TOPLEFT", 38, -400)
AdventureJournalOptionsPage_TooltipHeader:SetText("Tooltip Options")
AdventureJournalOptionsPage_TooltipHeader:SetFont("Fonts\\FRIZQT__.TTF", 14)
AdventureJournalOptionsPage_TooltipHeader:SetTextColor(1, 1, 1, 1)

AJ.TooltipLinesDropDown = CreateFrame("FRAME", "AJ_TooltipLinesDropDown", AJ.OptionsPanel, "UIDropDownMenuTemplate")
AJ.TooltipLinesDropDown:SetPoint("TOPLEFT", 26, -420)
AJ.TooltipLinesDropDown.displayMode = "MENU"
UIDropDownMenu_SetWidth(AJ.TooltipLinesDropDown, 140)
UIDropDownMenu_SetText(AJ.TooltipLinesDropDown, 'Tooltip Data')

-- NOTES COLOUR SELECTOR
local AdventureJournalOptionsPage_NotesHeader = AJ.OptionsPanel:CreateFontString("AJ_OptionsPageHeader_Tooltip", "OVERLAY", "GameFontNormal_NoShadow")
AdventureJournalOptionsPage_NotesHeader:SetPoint("TOPLEFT", 38, -320)
AdventureJournalOptionsPage_NotesHeader:SetText("Notes Options")
AdventureJournalOptionsPage_NotesHeader:SetFont("Fonts\\FRIZQT__.TTF", 14)
AdventureJournalOptionsPage_NotesHeader:SetTextColor(1, 1, 1, 1)

AJ.NoteColourDropdown = CreateFrame("FRAME", "AJ_NotesColourDropDown", AJ.OptionsPanel, "UIDropDownMenuTemplate")
AJ.NoteColourDropdown:SetPoint("TOPLEFT", 26, -340)
AJ.NoteColourDropdown.displayMode = "MENU"
UIDropDownMenu_SetWidth(AJ.NoteColourDropdown, 140)
UIDropDownMenu_SetText(AJ.NoteColourDropdown, 'Note Colour')

local AdventureJournalOptionsPage_NotesColourBoxDesc = AJ.OptionsPanel:CreateFontString("AJ_OptionsPageHeader_Tooltip", "OVERLAY", "GameFontNormal_NoShadow")
AdventureJournalOptionsPage_NotesColourBoxDesc:SetPoint("TOPLEFT", 220, -348)
AdventureJournalOptionsPage_NotesColourBoxDesc:SetText("Click to change colour")
AdventureJournalOptionsPage_NotesColourBoxDesc:SetFont("Fonts\\FRIZQT__.TTF", 12)
AdventureJournalOptionsPage_NotesColourBoxDesc:SetTextColor(1, 1, 1, 1)

AJ.NoteColourButtonFrame = CreateFrame("BUTTON", AJ_NoteColourButton, AJ.OptionsPanel) 
AJ.NoteColourButtonFrame:SetPoint('TOPLEFT', 365, -342)
AJ.DrawColourBox(AJ.NoteColourButtonFrame, 1, 1, 1, 40, 24)
AJ.NoteColourButtonFrame:SetScript('OnClick', function(self) AJ.UIColourBox = self AJ.ShowColourPicker(AJ.ColourPickerDataTable.RGB.r, AJ.ColourPickerDataTable.RGB.g, AJ.ColourPickerDataTable.RGB.b, 1.0, AJ.ColourPickerCallback)  end)


AJ.MinimapIconSizeSlider = CreateFrame("Slider", "AJ_MinimapIconSizeSlideBar", AJ.OptionsPanel, "OptionsSliderTemplate")
AJ.MinimapIconSizeSlider:SetThumbTexture("Interface/Buttons/UI-SliderBar-Button-Horizontal")
AJ.MinimapIconSizeSlider:SetSize(160, 20)
AJ.MinimapIconSizeSlider:SetOrientation('HORIZONTAL')
AJ.MinimapIconSizeSlider:SetPoint('TOPLEFT', 230, -115)
AJ.MinimapIconSizeSlider:SetMinMaxValues(4, 20)
AJ.MinimapIconSizeSlider:SetValue(8)
AJ.MinimapIconSizeSlider:SetValueStep(1)
--get value when changed
AJ.MinimapIconSizeSlider:SetScript('OnValueChanged', function(self) 
    _G[AJ.MinimapIconSizeSlider:GetName() .. 'Text']:SetText(tostring('Icon Size: '..string.format('%.0f', tostring(self:GetValue()))))
    if AJ2_GlobalVariables[UnitGUID('player')].Settings == nil then
        AJ2_GlobalVariables[UnitGUID('player')].Settings = {}
        AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapIconSize = tonumber(self:GetValue())
    else
        AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapIconSize = tonumber(self:GetValue())
    end
    AJ.UpdateMaps()
end)
AJ.MinimapIconSizeSlider.tooltipText = 'Adjust the icon size for World Map and Minimap' --Creates a tooltip on mouseover.
getglobal(AJ.MinimapIconSizeSlider:GetName()..'Low'):SetText('4')
getglobal(AJ.MinimapIconSizeSlider:GetName()..'High'):SetText('20')
getglobal(AJ.MinimapIconSizeSlider:GetName()..'Text'):SetText('Icon Size')

AJ.UseMinimapDistanceTrackingCheckBox = CreateFrame("CheckButton", 'AJ_UseMinimapDistanceTrackingCheckBox', AJ.OptionsPanel, "ChatConfigCheckButtonTemplate")
AJ.UseMinimapDistanceTrackingCheckBox:SetPoint('TOPLEFT', 230, -190)
AJ_UseMinimapDistanceTrackingCheckBoxText:SetText('|cffffffffUse distance tracking on minimap|r')
AJ_UseMinimapDistanceTrackingCheckBoxText:SetSize(150, 30)
AJ.UseMinimapDistanceTrackingCheckBox:SetScript('OnClick', function(self) 
    if AJ2_GlobalVariables[UnitGUID('player')].Settings == nil then
        AJ2_GlobalVariables[UnitGUID('player')].Settings = {}
        AJ2_GlobalVariables[UnitGUID('player')].Settings.UseMinimapDistanceTracking = self:GetChecked()
    else
        AJ2_GlobalVariables[UnitGUID('player')].Settings.UseMinimapDistanceTracking = self:GetChecked()
    end
    if AJ2_GlobalVariables[UnitGUID('player')].Settings.UseMinimapDistanceTracking == false then
        for k, node in ipairs(AJ.MapNodeCacheMinimap) do
            node:SetAlpha(1.0)
        end
    end
    AJ.UpdateMaps()
end)
AJ.UseMinimapDistanceTrackingCheckBox.tooltip = 'Fades minimap icons within a set distance, CONSUMES RESOURCES!.'

AJ.HideMinimapPinDistanceSlider = CreateFrame("Slider", "AJ_HideMinimapPinDistanceSlider", AJ.OptionsPanel, "OptionsSliderTemplate")
AJ.HideMinimapPinDistanceSlider:SetThumbTexture("Interface/Buttons/UI-SliderBar-Button-Horizontal")
AJ.HideMinimapPinDistanceSlider:SetSize(160, 20)
AJ.HideMinimapPinDistanceSlider:SetOrientation('HORIZONTAL')
AJ.HideMinimapPinDistanceSlider:SetPoint('TOPLEFT', 230, -160)
AJ.HideMinimapPinDistanceSlider:SetValue(8)
AJ.HideMinimapPinDistanceSlider:SetMinMaxValues(0.1, 4.5)
AJ.HideMinimapPinDistanceSlider:SetValueStep(0.05)
--get value when changed
AJ.HideMinimapPinDistanceSlider:SetScript('OnValueChanged', function(self) 
    _G[AJ.HideMinimapPinDistanceSlider:GetName() .. 'Text']:SetText(tostring('View Distance: '..string.format('%.2f', tostring(self:GetValue()))))
    if AJ2_GlobalVariables[UnitGUID('player')].Settings == nil then
        AJ2_GlobalVariables[UnitGUID('player')].Settings = {}
        AJ2_GlobalVariables[UnitGUID('player')].Settings.HideMinimapPinDistance = tonumber(self:GetValue())
    else
        AJ2_GlobalVariables[UnitGUID('player')].Settings.HideMinimapPinDistance = tonumber(self:GetValue())
    end
end)
AJ.HideMinimapPinDistanceSlider.tooltipText = 'Minimap View Distance, adjusts the distance at which icons disappear' --Creates a tooltip on mouseover.
getglobal(AJ.HideMinimapPinDistanceSlider:GetName()..'Low'):SetText('4')
getglobal(AJ.HideMinimapPinDistanceSlider:GetName()..'High'):SetText('20')
getglobal(AJ.HideMinimapPinDistanceSlider:GetName()..'Text'):SetText('View Distance')



function AJ.TooltipLinesDropDown_Init()
	UIDropDownMenu_Initialize(AJ.TooltipLinesDropDown, function(self, level, menuList)
		local info = UIDropDownMenu_CreateInfo()
		for k, l in ipairs(AJ2_GlobalVariables[UnitGUID('player')].Settings.TooltipLines) do
			info.text = l.DisplayText
			info.arg1 = l.Arg
			info.func = function() l.ShowLine = not l.ShowLine end
			info.checked = l.ShowLine
			info.isNotRadio = true
			UIDropDownMenu_AddButton(info)
		end
	end)
end

function AJ.NotesColourDropDown_Init()
	UIDropDownMenu_Initialize(AJ.NoteColourDropdown, function(self, level, menuList)
		local info = UIDropDownMenu_CreateInfo()
		for k, note in ipairs(AJ2_GlobalVariables[UnitGUID('player')].Settings.NoteColours) do
			info.text = note.DropdownText
			info.arg1 = note.NoteID
			info.func = function() UIDropDownMenu_SetText(AJ.NoteColourDropdown, note.DropdownText) AJ.NoteColourButtonFrame:SetBackdropColor(note.RGB.r, note.RGB.g, note.RGB.b, note.RGB.a) AJ.ColourPickerDataTable = note end
			info.isNotRadio = true
			UIDropDownMenu_AddButton(info)
		end
	end)
end

function AJ.MinimapDisplayObjectDropdown_Init()
	UIDropDownMenu_Initialize(AJ.MinimapDisplayObjectDropdown, function(self, level, menuList)
	local info = UIDropDownMenu_CreateInfo()
		local objs = {}
		if (level or 1) == 1 then
			for k, loot in ipairs(AJ2_GlobalVariables.Loot) do
				if string.find(loot.SourceGUID, 'GameObject') and (loot.ItemTypeID == 7 or loot.ItemTypeID == 4) then -- <-this keeps it to trade goods ||||||||||||||||||||||||||||||||| -- and loot.MapZoneName == GetMinimapZoneText() then
					if objs == nil then
						table.insert(objs, loot.SourceName)
						info.text = loot.SourceName
						info.arg1 = loot.SourceName
						info.arg2 = nil						
						info.func = function() 
							AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapDisplayObjects[loot.SourceName] = not AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapDisplayObjects[loot.SourceName] 
							AJ.UpdateMaps()
							local ticker = C_Timer.After(1, function() AJ.UpdateMaps() end)
						end
						info.checked = AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapDisplayObjects[loot.SourceName]
						info.isNotRadio = true
						--info.hasArrow = true
						--info.menuList = class.MenuList
						UIDropDownMenu_AddButton(info)
					else
						local add = true
						for k, obj in pairs(objs) do
							if obj == loot.SourceName then
								add = false
							end
						end
						if add == true then
							table.insert(objs, loot.SourceName)
							info.text = loot.SourceName
							info.arg1 = loot.SourceName
							info.arg2 = nil
							info.func = function() 
								AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapDisplayObjects[loot.SourceName] = not AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapDisplayObjects[loot.SourceName] 
								AJ.UpdateMaps() 
								local ticker = C_Timer.After(1, function() AJ.UpdateMaps() end)
							end
							info.checked = AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapDisplayObjects[loot.SourceName]
							info.isNotRadio = true
							--info.hasArrow = true
							--info.menuList = class.MenuList
							UIDropDownMenu_AddButton(info)
						end
					end
				end
			end
		end
	end)
end



--old function?
function AJ_MinimapDisplayObjectDropdown_OnClick(info, arg1, checked)
	print(text, arg1, arg2, checked)
	if checked == true then
		AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapDisplayObjects[arg1] = checked
	else
		AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapDisplayObjects[arg1] = checked
	end
	print(AJ2_GlobalVariables[UnitGUID('player')].Settings.MinimapDisplayObjects[arg1])
end