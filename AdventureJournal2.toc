## Interface: 11300
## Title: |cff0070DEAdventureJournal2|r
## Author: stpain (Aethroes & Thornbow @ Pyrewood Villae)
## Version: 1.0.3
## Notes: AdventureJournal2 is an RP addon that allows you journal your journey through Azeroth
## SavedVariables: AJ2_GlobalVariables

Libs\LibStub\LibStub.lua
Libs\CallbackHandler-1.0\CallbackHandler-1.0.lua
Libs\LibDataBroker-1.1\LibDataBroker-1.1.lua
Libs\LibDbIcon-1.0\LibDbIcon-1.0.lua
Libs\HereBeDragons\HereBeDragons-2.0.lua
Libs\HereBeDragons\HereBeDragons-Pins-2.0.lua

AdventureJournal2.lua

AdventureJournal2_Data.lua
AdventureJournal2_Util.lua

AdventureJournal2_Dialogs.lua
AdventureJournal2_Bags.lua
AdventureJournal2_Maps.lua
AdventureJournal2_Notes.lua
AdventureJournal2_Loot.lua
AdventureJournal2_CharacterStatistics.lua

AdventureJournal2_Options.lua

AdventureJournal2.xml