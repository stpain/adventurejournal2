local name, AJ = ...

function AJ.OpenJournal()
	if AdventureJournal:IsVisible() == true then
		AdventureJournal:Hide()
	else
		AdventureJournal:Show()
		AdventureJournalBagsFrame:Hide()
		AdventureJournalLootFrame:Hide()
		AdventureJournalCharacterStatisticsFrame:Hide()
		AdventureJournalNotesFrame:Show()
	end
end


function AJ.GetGameObjectDrops(obj)
	local drops = {}
	for k, loot in ipairs(AJ2_GlobalVariables.Loot) do
		local inRange, distance = AJ.IsNodeInRange(obj.MapZonePosX, obj.MapZonePosY, loot.MapZonePosX, loot.MapZonePosY, 0.3)
		if (inRange == true) and (string.find(loot.SourceGUID, 'GameObject')) then
			table.insert(drops, loot)
		end
	end
	return drops
end

--colour_box:SetScript('OnClick', function() Druidism.ColourPickerDataTable = cooldown colour_box_var = colour_box DruidHealing.show_color_picker(cd.RGB.r, cd.RGB.g, cd.RGB.b, 0.9, DruidHealing.color_picker_callback) end)
function AJ.ShowColourPicker(r, g, b, a, callback)
	ColorPickerFrame.hasOpacity, ColorPickerFrame.opacity = (a ~= nil), a
	ColorPickerFrame.previousValues = {r, g, b, a}
	ColorPickerFrame.func = callback
	ColorPickerFrame.opacityFunc = callback
	ColorPickerFrame.cancelFunc = callback
	ColorPickerFrame:SetColorRGB(r, g, b)
	ColorPickerFrame:Hide()
	ColorPickerFrame:Show()
end

function AJ.ColourPickerCallback(old_rgb)
	local new_R, new_G, new_B, new_A
	if old_rgb then
		new_R, new_G, new_B, new_A = unpack(old_rgb)
	else
		new_A = OpacitySliderFrame:GetValue()
		new_R, new_G, new_B = ColorPickerFrame:GetColorRGB()
		AJ.ColourPickerDataTable.RGB.r, AJ.ColourPickerDataTable.RGB.g, AJ.ColourPickerDataTable.RGB.b, AJ.ColourPickerDataTable.RGB.a = new_R, new_G, new_B, new_A
		AJ.UIColourBox:SetBackdropColor(new_R, new_G, new_B, new_A)
    end
end

--AJ.UpdateBarColourSettings(AJ.ColourPickerDataTable.NoteID, AJ.ColourPickerDataTable.RGB.r, AJ.ColourPickerDataTable.RGB.g, AJ.ColourPickerDataTable.RGB.b, AJ.ColourPickerDataTable.RGB.a)	

--dont think is used anymore
function AJ.UpdateNoteColourSettings(note, r, g, b, a)
	for k, note in ipairs(AJ2_GlobalVariables[UnitGUID('player')].Settings.NoteColours) do
		if note.NoteID == note then
			note.RGB.r, note.RGB.g, note.RGB.b, note.RGB.a = r, g, b, a
        end
	end
end

function AJ.DrawColourBox(frame, r, g, b, width, height)
	frame:SetBackdrop({bgFile = "Interface/Tooltips/UI-Tooltip-Background", 
												edgeFile = "Interface/Tooltips/UI-Tooltip-Border", 
												tile = true, tileSize = 8, edgeSize = 8, 
												insets = { left = 2, right = 2, top = 2, bottom = 2 }});
	frame:SetBackdropColor(r, g, b, 1)
	frame:SetSize(width, height)
end

function AJ.CreateListView(parentFrame, globalName, width, height, anchor, offsetX, offsetY)
	local f = CreateFrame("SCROLLFRAME", globalName, parent)
	f:SetPoint(anchor, offsetX, offsetY)
	f:SetSize(width, height)
	f:SetBackdropColor(0, 0, 0, 1)

	f.scrollFrame = CreateFrame("Slider", globalName..'_ScrollFrame', f, "UIPanelScrollBarTemplate")
	f.scrollFrame:SetPoint("TOPLEFT", f, "TOPRIGHT", -24,-12) 
	f.scrollFrame:SetPoint("BOTTOMLEFT", f, "BOTTOMRIGHT", -24, 20)
	f.scrollFrame:SetMinMaxValues(1, 10)
	f.scrollFrame:SetValueStep(1.0)
	f.scrollFrame.scrollStep = 1
	f.scrollFrame:SetValue(1)
	f.scrollFrame:SetWidth(16)
end

-- need to check apple formatting
function AJ.GetDateFormatted(dateObj)
	if not dateObj then
		if IsWindowsClient() then
			return date("%d %B %Y")
		elseif IsLinuxClient() then
			return date("%a %b %d")
		end
	else
		if IsWindowsClient() then
			return date("%d %B %Y", time(dateObj))
		elseif IsLinuxClient() then
			return date("%a %b %d", time(dateObj))
		end
	end
end

function AJ.GetTimeFormatted(dateObj)
	if not dateObj then
		if IsWindowsClient() then
			return date('%H:%M:%S')
		elseif IsLinuxClient() then
			return date('%H:%M:%S')
		end
	else
		if IsWindowsClient() then
			return date('%H:%M:%S', time(dateObj))
		elseif IsLinuxClient() then
			return date('%H:%M:%S', time(dateObj))
		end
	end
end

function AJ.AddQuestXpData(questID, xp)
	if AJ2_GlobalVariables.QuestXP == nil then
		AJ2_GlobalVariables.QuestXP = {}
	end
	AJ2_GlobalVariables.QuestXP[questID] = xp
end

function AJ.MakeFrameMove(frame)
	frame:SetMovable(true)
	frame:EnableMouse(true)
	frame:RegisterForDrag("LeftButton")
	frame:SetScript("OnDragStart", frame.StartMoving)
	frame:SetScript("OnDragStop", frame.StopMovingOrSizing)
end

function AJ.GetArgs(...)
	for i=1, select("#", ...) do
		arg = select(i, ...)
		print(i.." "..tostring(arg))
	end
end

function AJ.Print(msg, override)
	--if ForagerGlobalSettings[UnitGUID('player')].PrintForagerInfo == true or override == true then
		print('|cff00FF96Adventure Journal 2: |r'..msg)	
	--end
end

function AJ.PrintMoney(msg, money)
	print('|cff00FF96Adventure Journal 2: |r'..msg..' '..GetCoinTextureString(money))
end

AJ.TooltipLineAdded = false
function AJ.OnTooltipSetItem(tooltip, ...)
    local ItemName, ItemLink = GameTooltip:GetItem()
    if ItemLink then
        local item = {}
		--local item[1], item[2], item[3], item[4], item[5], item[6], item[7], item[8], item[9], item[10], item[11], item[12], item[13], item[14], _, item[15], item[16] = GetItemInfo(ItemLink)
  
		if not AJ.TooltipLineAdded then
			tooltip:AddLine(' ') --create a line break
            tooltip:AddLine("|cff0070DETooltip Classic|r")
            
            for k, l in ipairs(AJ2_GlobalVariables[UnitGUID('player')].Settings.TooltipLines) do
                if l.ShowLine == true then
                    if l.DisplayText == 'Item Sell Price' then
                        --tooltip:AddDoubleLine(l.DisplayText, GetCoinTextureString(item[l.Arg]), 1, 1, 1, 1, 1, 1)
						local v = select(l.Arg, GetItemInfo(ItemLink))
						if type(v) == 'number' then
							tooltip:AddDoubleLine(l.DisplayText, GetCoinTextureString(v))
						end
                    else
                        --tooltip:AddDoubleLine(l.DisplayText, item[l.Arg], 1, 1, 1, 1, 1, 1)
                        tooltip:AddDoubleLine(l.DisplayText, select(l.Arg, GetItemInfo(ItemLink)), 1, 1, 1, 1, 1, 1)
                    end
                end
			end
			local c, d = 0, 0
			if GetItemCount(ItemLink) ~= nil then
				c = GetItemCount(ItemLink)
			end
			if select(11, GetItemInfo(ItemLink)) ~= nil then
				d = select(11, GetItemInfo(ItemLink))
			end
			local v = d * c
			if type(v) == 'number' then
				tooltip:AddDoubleLine('Stack Value', GetCoinTextureString(v))
			end
			AJ.TooltipLineAdded = true
		end

    end
end

function AJ.OnTooltipCleared(tooltip, ...)
    AJ.TooltipLineAdded = false
end

GameTooltip:HookScript("OnTooltipSetItem", AJ.OnTooltipSetItem)
GameTooltip:HookScript("OnTooltipCleared", AJ.OnTooltipCleared)