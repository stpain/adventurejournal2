local name, AJ = ...

AJ.NotesItemTitle = { 
	['quest'] = '|cff005100 Quest accepted |r',
	['note'] = '|cff510000',
	['discovered'] = '|cff003366 ',
	['quest-completed'] = '|cff46008a Quest completed |r'
}

AJ.NoteColours = { 
	{ DropdownText = 'Quest', NoteID = 'quest', RGB = { r = 1, g = 1, b = 1, } },
	{ DropdownText = 'Quest Completed', NoteID = 'quest-completed', RGB = { r = 1, g = 1, b = 1, } },
	{ DropdownText = 'Discovery', NoteID = 'discovered', RGB = { r = 1, g = 1, b = 1, } },
	{ DropdownText = 'Note', NoteID = 'note', RGB = { r = 1, g = 1, b = 1, } },
}

AJ.NoteTracking = {
	['quest'] = true,
	['discovery'] = true,
	['note'] = true,
}

AJ.TooltipLines = {
    { DisplayText = 'Item Name', ShowLine = false, Arg = 1 }, 
    { DisplayText = 'Item Link', ShowLine = false, Arg = 2 }, 
    { DisplayText = 'Item Rarity', ShowLine = false, Arg = 3 }, 
    { DisplayText = 'Item Level', ShowLine = false, Arg = 4 }, 
    { DisplayText = 'Item Min Level', ShowLine = false, Arg = 5 }, 
    { DisplayText = 'Item Type', ShowLine = false, Arg = 6 }, 
    { DisplayText = 'Item Sub Type', ShowLine = false, Arg = 7 }, 
    { DisplayText = 'Item Max Stack Count', ShowLine = false, Arg = 8 }, 
    { DisplayText = 'Item Equip Location', ShowLine = false, Arg = 9 }, 
    { DisplayText = 'Item Icon', ShowLine = false, Arg = 10 }, 
    { DisplayText = 'Item Sell Price', ShowLine = false, Arg = 11 }, 
    { DisplayText = 'Item Class ID', ShowLine = false, Arg = 12 }, 
    { DisplayText = 'Item Sub Class ID', ShowLine = false, Arg = 13 }, 
    { DisplayText = 'Binding Type', ShowLine = false, Arg = 14 }, 
    { DisplayText = 'Item Set ID', ShowLine = false, Arg = 15 }, 
    { DisplayText = 'Crafting Reagent', ShowLine = false, Arg = 16 },
}

AJ.FishingSpells = { 271616, 272011, 271675, 271672, 271658, 271656, 271662, 271677, 131490, 131476, 131474, 7620, 158743, 1104110, 88868, 51294, 33095, 18248, 7732, 7731, 271664, 271660 }
AJ.SpellID = { 2575, 2576, 3564, 10248, 29354, 50310, 74517, 102161, 158754, 195122, 265837, 2366, 2368, 3570, 11993, 28695, 50300, 74519, 110413, 158745, 195114, 265819, 3365 }
AJ.MinimapScales = { [0] = 7.0, [1] = 9.0, [2] = 11.5, [3] = 14.0, [4] = 18.0, [5] = 27.0 }

AJ.ClassColours = {
	DEATHKNIGHT = { r = 0.77, g = 0.12, b = 0.23, fs = '|cffC41F3B' },
	DRUID = { r = 1.00, g = 0.49, b = 0.04, fs = '|cffFF7D0A' },
	HUNTER = { r = 0.67, g = 0.83, b = 0.45, fs = '|cffABD473' },
	MAGE = { r = 0.25, g = 0.78, b = 0.92, fs = '|cff40C7EB' },
	PALADIN = { r = 0.96, g = 0.55, b = 0.73, fs = '|cffF58CBA' },
	PRIEST = { r = 1.00, g = 1.00, b = 1.00, fs = '|cffFFFFFF' },
	ROGUE = { r = 1.00, g = 0.96, b = 0.41, fs = '|cffFFF569' },
	SHAMAN = { r = 0.00, g = 0.44, b = 0.87, fs = '|cff0070DE' },
	WARLOCK = { r = 0.53, g = 0.53, b =	0.93, fs = '|cff8787ED' },
	WARRIOR = { r = 0.78, g = 0.61, b = 0.43, fs = '|cffC79C6E' },
	Total = { r = 1, g = 1, b = 1 }
}

AJ.ItemTypesDb = {
	{ TypeValue = 0 , DisplayName = 'Consumable' },
	{ TypeValue = 1 , DisplayName = 'Container' },
	{ TypeValue = 2 , DisplayName = 'Weapon' },
	{ TypeValue = 3 , DisplayName = 'Gem' },
	{ TypeValue = 4 , DisplayName = 'Armor' },
	{ TypeValue = 5 , DisplayName = 'Reagent' },
	{ TypeValue = 6 , DisplayName = 'Projectile' },
	{ TypeValue = 7 , DisplayName = 'Tradeskill' },
	{ TypeValue = 8 , DisplayName = 'Item Enhancement' },
	{ TypeValue = 9 , DisplayName = 'Recipe' },
	--{ TypeValue = 10 , DisplayName = 'Money(OBSOLETE)' },
	{ TypeValue = 11 , DisplayName = 'Quiver' },
	{ TypeValue = 12 , DisplayName = 'Quest' },
	{ TypeValue = 13 , DisplayName = 'Key' },
	--{ TypeValue = 14 , DisplayName = 'Permanent(OBSOLETE)' },
	{ TypeValue = 15 , DisplayName = 'Miscellaneous' },
	--{ TypeValue = 16 , DisplayName = 'Glyph' },
	--{ TypeValue = 17 , DisplayName = 'Battle Pets' },
	--{ TypeValue = 18 , DisplayName = 'WoW Token' },
}

AJ.CharacterStatistics = {
	{ Key = 'Character Deaths', Value = 0 },
	{ Key = 'Total Gold Earned', Value = 0 },
	{ Key = 'Food Eaten', Value = 0 },
	{ Key = 'Drinks Consumed', Value = 0 },
	{ Key = 'Gold Spent on Repairs', Value = 0 },
	{ Key = 'Quests Completed', Value = 0 },
	{ Key = 'Quests Accepted', Value = 0 },
	{ Key = 'Quests Abandoned', Value = 0 },
	{ Key = 'Fishing Items Caught', Value = 0 },
	{ Key = 'Total Gold Looted', Value = 0 },
	{ Key = 'Bandages Used', Value = 0 },
}

--use this to set the display order for character stats
AJ.CharStatsRowIDs = {
	['Character Deaths'] = 1,
	['Drinks Consumed'] = 2,
	['Food Eaten'] = 3,
	['Bandages Used'] = 4,
	['Gold Spent on Repairs'] = 5,
	['Fishing Items Caught'] = 6,	
	['Quests Accepted'] = 7,
	['Quests Completed'] = 8,
	['Quests Abandoned'] = 9,
	['Total Gold Earned'] = 10,
	['Total Gold Looted'] = 11,
}